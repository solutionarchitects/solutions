__author__ = 'Sisense'

#login page
username_selector = 'input[data-ng-model=username]'
password_selector = 'input[data-ng-model=password]'
login_button_selector = 'button.acc-btn'

#home page
home_page_selector = "div#home-toolbar"

#dashboard loaded
dashboard_loaded_selector = 'div.dashboard-header'

#widget loading
widget_loading_css = "widget-overlay"

#edit filter
edit_filter_clickable_selector_xpath = "//div[@title = 'Edit Filter']"

#filter menu open
filter_items_selector_xpath = "//div[@class='popup-inner']//span[contains(text(),'Filter')]"
list_filter_xpath = "//div[@class='uc-nav']//div[position()=1]"

#filter selection one or many
select_mode_one_xpath = "//div[contains(@class, 'uc-selectmode-one')]"
select_mode_many_xpath = "//div[contains(@class, 'uc-selectmode-many')]"

#filter items
unchecked_xpath = "//div[@class='uc-filter']//div[contains(@class, 'uc-chk-icon-empty')]"
checked_xpath = "//div[@class='uc-filter']//div[contains(@class, 'uc-chk-icon-check')]"
filter_ok_button_xpath = "//div[@class='uc-ok']"

#error on page
error_on_page_css = 'div[data-ng-include="errorTemplate"]'