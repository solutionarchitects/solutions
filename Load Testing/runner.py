import subprocess
import os
import time
from pymongo import MongoClient
import uuid
import user_defined_settings

guid = uuid.uuid4()

test_filename = 'test.py'
processes = []
working_directory = os.path.dirname(os.path.realpath(__file__))
mongod = working_directory + '\\mongodb\\bin\\mongod.exe'
mongodb = working_directory + '\\mongodb\\DB'

mongo_p = subprocess.Popen([mongod, '--dbpath', mongodb, '--port', '27019'])

for index, test in enumerate(range(user_defined_settings.number_of_concurrent_users)):

    if index % 2 == 0:
        browser = 'chrome'
    elif index % 2 == 1:
        browser = 'firefox'
    # elif index % 11 == 0:
    #     browser = 'ie'

    processes.append(subprocess.Popen(['python', test_filename,
                                       browser,
                                       str(user_defined_settings.number_of_concurrent_users),
                                       str(guid),
                                       str(user_defined_settings.time_to_run_test),
                                       str(user_defined_settings.ramp_up_time)]))

    time.sleep(user_defined_settings.ramp_up_time)

    current_time = float(time.time())
    if current_time > user_defined_settings.end_test_time:
        break

for process in processes:
    process.wait()

client = MongoClient(user_defined_settings.mongodb_connection_uri)
load_testing_db = client.load_testing
events_collection = load_testing_db.dashboard_events

events = events_collection.find()
client.close()

for event in events:
    print event




