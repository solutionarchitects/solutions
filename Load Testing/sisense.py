__author__ = 'Sisense'

import time
import json
import jwt
import urllib
import requests

REST_API_TOKEN = 'cec87d105334143a19330c7e6bfc2a38'
SSO_SHARED_SECRET = '4383b39733228eff804dbaef7c72aba7'

URL_HUB = "http://192.168.5.40:4444/wd/hub"
BASE_URL = "http://sibi.sisense.com/"
MONGODB_URI = 'mongodb://localhost:27019'
LOGIN = "app/account#/login/"
DASHBOARD = "app/main#/dashboards/"

USERNAME = 'eli.pridonoff@sisense.com'
PASSWORD = 'eli1234'

# sql = {'query': 'SELECT * from Hours_Report', 'format': 'csv'}
# elasticube_name = "SalesForce"

def create_jwt_token(for_api=False):

    payload = {
        "iat": int(time.time())
    }

    if for_api:
        shared_key = REST_API_TOKEN
        payload['email'] = USERNAME
        payload['password'] = PASSWORD
    else:
        shared_key = SSO_SHARED_SECRET
        payload['sub'] = USERNAME


    jwt_string = jwt.encode(payload, shared_key)
    encoded_jwt = urllib.quote_plus(jwt_string) # url-encode the jwt string

    return encoded_jwt

def call_api(http_method, url_suffix, params=None, payload=None):

    encoded_jwt = create_jwt_token(for_api=True)
    headers = {'x-api-key': encoded_jwt}

    if payload:
        headers['Content-Type'] = 'application/json'

    url = BASE_URL + url_suffix

    response = None
    data = None

    if http_method == 'GET':
        response = requests.get(url, params=params, headers=headers)
    elif http_method == 'POST':
        data = json.dumps(payload)
        response = requests.post(url, data=data, params=params, headers=headers)

    return response

def get_dashboard_details(url):
    res = call_api('GET', url)
    res_json = json.loads(res.text)

    widgets = res_json.get('widgets')
    global_filters = res_json.get('filters')
    title = res_json.get('title')

    return (widgets, global_filters, title)

def get_all_dashboards():

    res = call_api('GET', '/api/dashboards')
    return json.loads(res.text)

def get_all_dashboard_ids_filtered_by_name(dashboard_names):

    dashboard_ids = []
    dashboards = get_all_dashboards()

    for dashboard in dashboards:
        if dashboard.get('title') in dashboard_names:
            dashboard_ids.append(dashboard.get('oid'))

    return dashboard_ids

def add_user(email, consumer=False, contributor=True, admin=False):

    payload = [
        {
            "email": email,
            "firstName": "",
            "lastName": "",
            "roles": {"contributor": contributor, "consumer": consumer, "admin": admin},
            "hash": "cb7f9219fabe98ed5bb5b32c5ce2854c",
            'active': True
        }
    ]

    res = call_api('POST', '/api/users', payload=payload)
    if res.reason == 'OK':
         return json.loads(res.text)

def share_dashboard(email, dashboard):
    print 'test'

add_user('eli.pridonoff@gmail.com')