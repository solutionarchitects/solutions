from selenium import selenium
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import *
import random
import time
import csv
import sys
from pymongo import MongoClient
import dom
import requests
import sisense
import json
import user_defined_settings

def check_for_error(driver, error_element_selector, selector_type):

    try:
        if selector_type == 'css':
            return True, driver.find_element(By.CSS_SELECTOR, error_element_selector).text

    except NoSuchElementException:
        pass

    return False, ''

def write_results_to_mongo(mongodb_uri, db, collection, payload):
    client = MongoClient(mongodb_uri)
    db = client[db]
    collection = db[collection]
    collection.insert(payload)
    client.close()

def test_login(username, password, username_selector, password_selector, driver, times, results):
    try:
        el = WebDriverWait(driver, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, username_selector))
        )
    except NoSuchElementException:
        print "username element not found"
        driver.quit()
        results['test_design_error_message'].append('Username element not found on page')
        return False

    username_el = driver.find_element_by_css_selector(username_selector)
    username_el.clear()
    username_el.send_keys(username)

    time.sleep(1)

    password_el = driver.find_element_by_css_selector(password_selector)
    password_el.clear()
    password_el.send_keys(password)

    button = driver.find_element_by_css_selector(dom.login_button_selector)
    button.click()

    times['login_time'] = time.time()

    try:
        el = WebDriverWait(driver, 20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, dom.home_page_selector))
        )
    except TimeoutException:
        print "Dashboard not loaded into dom"
        driver.quit()
        results['test_design_error_message'].append('Dashboard not loaded into dom')
        write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='dashboard_events', payload=results)
        return False

    times['logged_in_time'] = time.time()

    return True

def test_load_dashboard(driver, url, widgets, global_filters, times, results, test_run_number):

    def get_widget_results(widget):

        number_of_widget_filters = 0
        number_of_jaql_dimensions = 0

        for item in widget.get('metadata').get('panels'):
            if item.get('name') == 'filters':
                for filter in item['items']:
                    number_of_widget_filters += 1
                    number_of_jaql_dimensions += 1
            else:
                for other_item in item['items']:
                    number_of_jaql_dimensions += 1


        return {
            'guid': guid, 'browser': browser, 'url': url,
            'number_of_concurrent_users': number_of_concurrent_users, 'widget_loaded': time.time(),
            'widget_load_time': time.time() - times['logged_in_time'],
            'widget_type': widget.get('type'), 'widget_sub_type': widget.get('subtype'),
            'number_of_widgets_in_dashboard': len(widgets), 'dashboard_id': widget.get('dashboardid'),
            'dashboard_filters': 0 if not global_filters else len(global_filters), 'widget_filters': number_of_widget_filters,
            'number_of_jaql_dimensions': number_of_jaql_dimensions,
            'ramp_up_time': ramp_up_time,
            'dashboard_name': results['dashboard_name'],
            'test_run_number': test_run_number


        }

    widget_css_selectors = {}

    for index, widget in enumerate(widgets):
        widget_css_selectors["widget[widgetid=\"" + widget.get('_id') + "\"] widget-overlay"] = widget

    widget_css_selector_keys = []
    for widget_css_selector_key in widget_css_selectors:
        widget_css_selector_keys.append(widget_css_selector_key)

    driver.get(url)

    count_up = 0
    while count_up < 60 * 5:

        for widget_css_key in widget_css_selector_keys:
            try:
                driver.find_element_by_css_selector(widget_css_key)
            except NoSuchElementException:
                widget = widget_css_selectors[widget_css_key]
                widget_results = get_widget_results(widget)

                widget_css_selector_keys.remove(widget_css_key)
                write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='widget_events', payload=widget_results)

        try:
            widgets_loading = WebDriverWait(driver, 0.05).until(
                EC.invisibility_of_element_located((By.CSS_SELECTOR, dom.widget_loading_css))
            )
            break
        except TimeoutException:
            pass

        time.sleep(0.05)
        count_up += 0.05

    for remaining_widgets in widget_css_selector_keys:
        widget = widget_css_selectors[remaining_widgets]
        widget_results = get_widget_results(widget)
        write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='widget_events', payload=widget_results)


    error = False
    try:
        el = WebDriverWait(driver, 60*5).until(
            EC.invisibility_of_element_located((By.CSS_SELECTOR, dom.widget_loading_css))
        )
    except TimeoutException:
        error = True
        results['test_design_error_message'].append('Dashboard did not load after 5 minutes')
        print "Dashboard Not loaded after 5 minutes"
        write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='dashboard_events', payload=results)
        return False


    is_error, test_design_error_message = check_for_error(driver, dom.error_on_page_css, 'css')

    if is_error:
        results['elasticube_error_message'].append(test_design_error_message)
        write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='dashboard_events', payload=results)
        return False

    times['dashboard_loaded_time'] = time.time()

    if is_error or error:
        driver.quit()
        return False

    return True

def test_filter_menu_open(driver, times, results):

    try:
        edit_filter = driver.find_element_by_xpath(dom.edit_filter_clickable_selector_xpath)
        edit_filter.click()

    except NoSuchElementException:
        results['test_design_error_message'].append("Edit Filter Menu not found")
        print "Edit Filter Menu not found"
        write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='dashboard_events', payload=results)
        return False

    try:
        el = WebDriverWait(driver, 20).until(
            EC.presence_of_element_located((By.XPATH, dom.list_filter_xpath))
        )
    except (NoSuchElementException, TimeoutException):
        results['test_design_error_message'].append("List filter not found")
        print "List filter not found"
        driver.quit()
        write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='dashboard_events', payload=results)
        return False

    return True

def getBrowserType(browser):
    if browser == 'firefox':
        return DesiredCapabilities.FIREFOX
    elif browser == 'chrome':
        return DesiredCapabilities.CHROME
    elif browser == 'ie':
        return DesiredCapabilities.INTERNETEXPLORER
    else:
        sys.exit()

dashboard_ids = sisense.get_all_dashboard_ids_filtered_by_name(user_defined_settings.dashboard_names)

random.seed()

browser = sys.argv[1]
number_of_concurrent_users = sys.argv[2]
guid = sys.argv[3]
time_to_run_test = sys.argv[4]
ramp_up_time = sys.argv[5]

start_tests_time = user_defined_settings.start_test_time
end_tests_time = user_defined_settings.end_test_time

def run_test(test_run_number=0):
    for dashboard_id in dashboard_ids:
        
        results = {
            'guid': guid,
            'browser': browser,
            'url': sisense.BASE_URL + sisense.DASHBOARD + dashboard_id,
            'number_of_concurrent_users': number_of_concurrent_users,
            'start_time': None,
            'load_dashboard_filters': None,
            'first_page_load': None,
            'signin': None,
            'load_dashboard': None,
            'test_design_error_message': [],
            'number_of_widgets': None,
            'number_of_global_filters': None,
            'ramp_up_time': ramp_up_time,
            'dashboard_name': None,
            'test_run_number': test_run_number,
            'elasticube_error_message': None
        }

        times = {
            'start': None,
            'first_page_load': None,
            'login_time': None,
            'logged_in_time': None,
            'dashboard_loaded_time': None,
            'filters_selected': None,
            'dashboard_loading_time_after_filters_selected': None
        }

        driver = webdriver.Remote(
            command_executor=sisense.URL_HUB,
            desired_capabilities=getBrowserType(browser)

        )

        # Get Dashboard Details
        (widgets, global_filters, title) = sisense.get_dashboard_details('api/dashboards/' + dashboard_id)
        results['number_of_global_filters'] = 0 if not global_filters else len(global_filters)
        results['number_of_widgets'] = 0 if not widgets else len(widgets)
        results['dashboard_name'] = title

        # Open Home Page (Login)
        results['start_time'] = times['start_time'] = time.time()
        driver.get(sisense.BASE_URL)
        driver.maximize_window()

        try:
            el = WebDriverWait(driver, 20).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, dom.username_selector))
            )

        except (NoSuchElementException, TimeoutException):
            print "Home page not found"
            driver.quit()
            results['test_design_error_message'].append('Failed to Open First Page')
            write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='dashboard_events', payload=results)
            continue

        times['first_page_load'] = time.time()
        results['first_page_load'] = times['first_page_load'] - times['start_time']

        time.sleep(1)

        # Login to SiSense
        if not test_login(username=sisense.USERNAME, password=sisense.PASSWORD,
                   username_selector=dom.username_selector, password_selector=dom.password_selector, driver=driver, times=times, results=results):
            driver.quit()
            continue

        results['signin'] = times['logged_in_time'] - times['login_time']

        # Load Dashboard
        if not test_load_dashboard(driver, sisense.BASE_URL + sisense.DASHBOARD + dashboard_id, widgets, global_filters, times, results, test_run_number):
            driver.quit()
            continue

        results['load_dashboard'] = times['dashboard_loaded_time'] - times['logged_in_time']

        # Open Filter Menu
        if not test_filter_menu_open(driver, times, results):
            driver.quit()
            continue

        try:
            list_filter = driver.find_element_by_xpath(dom.list_filter_xpath)
        except NoSuchElementException:
            print "List filter not found"
            driver.quit()
            write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='dashboard_events', payload=results)
            continue


        list_filter.click()

        try:
            el = WebDriverWait(driver, 20).until(
                EC.presence_of_element_located((By.XPATH, dom.select_mode_many_xpath + ' | ' + dom.select_mode_one_xpath))
            )
        except (NoSuchElementException, TimeoutException):
            print "Filter items selector not found"
            driver.quit()
            results['test_design_error_message'].append('Filter items selector not found')
            write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='dashboard_events', payload=results)
            continue

        single_selector = multi_selector = []
        try:
            single_selector = driver.find_element_by_xpath(dom.select_mode_one_xpath)
        except NoSuchElementException:
            print "single_selector not found"


        try:
            multi_selector = driver.find_element_by_xpath(dom.select_mode_many_xpath)
        except NoSuchElementException:
            print "multi_selector not found"


        # if str(type(multi_selector)) != "<class 'selenium.webdriver.remote.webelement.WebElement'>" and \
        #    str(type(single_selector)) != "<class 'selenium.webdriver.remote.webelement.WebElement'>":
        #     print "single_selector and multi_selector not found"
        #     driver.quit()
        #     write_results_to_mongo(mongodb_uri=mongodb_uri, db='load_testing', collection='dashboard_events', payload=results)




        if str(type(multi_selector)) != "<class 'selenium.webdriver.remote.webelement.WebElement'>" and \
           str(type(single_selector)) == "<class 'selenium.webdriver.remote.webelement.WebElement'>":
            single_selector.click()


        try:
            el = WebDriverWait(driver, 20).until(
                EC.invisibility_of_element_located((By.XPATH, dom.select_mode_one_xpath))
            )
        except (NoSuchElementException, TimeoutException):
            print "Multiple Select of Filter Items not found"
            results['test_design_error_message'].append("Multiple Select of Filter Items not found")
            driver.quit()
            write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='dashboard_events', payload=results)
            continue


        checkbox_elements = driver.find_elements_by_xpath(dom.unchecked_xpath + ' | ' + dom.checked_xpath)


        for index, el in enumerate(checkbox_elements):
            if el.is_displayed():
                if random.random() > 0.5 and index is not 0:
                    el.click()

        button = driver.find_element_by_xpath(dom.filter_ok_button_xpath)
        button.click()

        times['filters_selected'] = time.time()

        try:
            el = WebDriverWait(driver, 60*5).until(
                EC.invisibility_of_element_located((By.CSS_SELECTOR, dom.widget_loading_css))
            )
        except (NoSuchElementException, TimeoutException):
            driver.quit()
            print "Dashboard Not loaded after 5 minutes"
            results['test_design_error_message'].append("Dashboard Not loaded after 5 minutes")
            write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='dashboard_events', payload=results)
            continue


        times['dashboard_loading_time_after_filters_selected'] = time.time()
        results['load_dashboard_filters'] = times['dashboard_loading_time_after_filters_selected'] - times['filters_selected']

        driver.quit()

        write_results_to_mongo(mongodb_uri=sisense.MONGODB_URI, db='load_testing', collection='dashboard_events', payload=results)
        continue

    current_time = float(time.time())
    if current_time < end_tests_time:
        run_test(test_run_number=test_run_number + 1)

run_test()

        # javascript = \
        #     'var dashboardFilters = $$get(prism, "$ngscope.dashboard.filters.$$items");' + \
        #     'var newJaql = {jaql: {}};' + \
        #     'newJaql.jaql = dashboardFilters[2].jaql;' + \
        #     "newJaql.jaql.filter.members.push('Omaha');" + \
        #     'prism.$ngscope.dashboard.filters.clear();' + \
        #     'prism.$ngscope.dashboard.filters.update(newJaql,{save:true, refresh:true});'
        #
        #
        # prism = driver.execute_script(javascript)

        # for dashboard in dashboards:
        #     driver.get(sisense + dashboard)




        # run_test()

