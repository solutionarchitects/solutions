import time

dashboard_names = ['SFDC', 'SFDC2']
number_of_concurrent_users = 1
# add number_of_concurrent_users every ramp_up_time seconds
ramp_up_time = 1

# seconds to run test
time_to_run_test = 10

# location of mongodb -- DO NOT TOUCH
mongodb_connection_uri = 'mongodb://localhost:27019'

# DO NOT TOUCH
start_test_time = float(time.time())
end_test_time = start_test_time + float(time_to_run_test)

initialize_hub = 'java -jar "C:\Users\Rob\Documents\solutions\Load Testing\selenium-server-standalone-2.44.0.jar" -port 4444 -role hub'
initialize_node = 'java -jar selenium-server-standalone-2.44.0.jar -role webdriver -hub http://192.168.5.40:4444/grid/register port 5566 -Dwebdriver.chrome.driver="c:\chromedriver.exe" -Dwebdriver.ie.driver="c:\IEDriverServer_x64.exe -maxSession 10 '
