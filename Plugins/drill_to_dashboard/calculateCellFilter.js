
    //get cell filters
    function getCellFilters(cell, widget){
        //create filter object
        function createFilter(items, fieldIndex, value){
            var item = {jaql:{}};
            var tmpitem = $.grep(items,function(i){return i.field.index == fieldIndex})[0];

            item.jaql.datatype = tmpitem.jaql.datatype;
            item.jaql.dim = tmpitem.jaql.dim;
            item.jaql.filter = {members : [value]};

            if (tmpitem.jaql.level) {
                item.jaql.level = tmpitem.jaql.level;
            }

            return item;
        }

        function isLowermostMeasureHeader(classString){
            return (classString.indexOf("p-measure-head") >=0 ||
            classString.indexOf("p-total-head") >= 0 &&
            classString.indexOf("p-dim-member") < 0 ||
            classString.indexOf("p-grand-total-head") >= 0);
        }

        function isLowermostHeaderClass(classString){
            return (classString.indexOf("p-dim-head") >= 0 || isLowermostMeasureHeader(classString));
        }

        function getLowermostHeaderCellsInOrder(table){
            function scanSubtree(rowIndex){
                for(;leavesToFindInCurrentSubtree[rowIndex] > 0;)
                    scanSubtree(rowIndex + rowSpansOfCurrentSubtree[rowIndex]);

                var indexOfCellWithinRow = cellIndices[rowIndex]
                var currentRow = headerRows[rowIndex];

                if(currentRow && !(currentRow.cells.length <= indexOfCellWithinRow)){
                    var numberOfLeavesInSubtree = parseInt(currentRow.cells[indexOfCellWithinRow].getAttribute("colspan") || 1);
                    var rowSpan = parseInt(currentRow.cells[indexOfCellWithinRow].getAttribute("rowspan") || 1);

                    if((numberOfLeavesInSubtree > 1 || !isLowermostHeaderClass(currentRow.cells[indexOfCellWithinRow].className)) && headerRows.length >= rowIndex + rowSpan + 1)
                        return leavesToFindInCurrentSubtree[rowIndex] = numberOfLeavesInSubtree || 1, cellIndices[rowIndex]++, rowSpansOfCurrentSubtree[rowIndex] = rowSpan, void scanSubtree(rowIndex+rowSpan);

                    elements.push(currentRow.cells[indexOfCellWithinRow]), cellIndices[rowIndex]++;

                    for(var i=0;rowIndex>i;++i)leavesToFindInCurrentSubtree[i]--;scanSubtree(0)}
            }

            for(var elements=[],headerRows=$(table).find("thead tr:not(.p-fake-measure-head)"),cellIndices=[],leavesToFindInCurrentSubtree=[],rowSpansOfCurrentSubtree=[],i=0;i<headerRows.length;++i)
                cellIndices.push(0),leavesToFindInCurrentSubtree.push(0),rowSpansOfCurrentSubtree.push(0);

            return scanSubtree(0),elements
        }

        // given a cell, gets the dimension member row cells for that cell, for all row dimensions. (i.e, for a value cell of row 'tel aviv -> israel -> asia', returns the cells for 'tel aviv', 'israel', 'asia')
        // cell can be either a value cell, or a dimension member cell
        function getRowCells(cell, dimensionsContainer) {
            var dimCellId = $(cell.parentNode.cells).filter('.p-dim-member:last, .p-total-row-head:last').prop('id'); //last dimension is guaranteed to be in the same row as value
            var currCell = cell.className.indexOf('p-dim-member') >= 0 ? cell : dimensionsContainer.find('td[originalid = "' + dimCellId + '"], td#' + dimCellId)[0];	// get corresponding cell in the dimensions container
            var allDimensionMemberCells = $(currCell).parents('tbody').find('td.p-dim-member');
            var rowCells = [];
            var lastFoundCell = currCell;
            var currCellIndex = parseInt(currCell.getAttribute('fIdx'));

            if (currCellIndex == 0) {
                return [currCell];
            }

            while (currCellIndex > 0) {
                var currRow = lastFoundCell.parentNode;

                while (!currCell) {
                    currRow = node_before(currRow);
                    currCell = $(currRow).find('td.p-dim-member[fIdx = ' + (currCellIndex - 1) + ']')[0];
                }

                while (currCell) {
                    currCellIndex = parseInt(currCell.getAttribute('fIdx'));

                    // find cells with the same value of the same field
                    var cellsWithSameValue = getCellsFromSameFieldWithSameTextValue(allDimensionMemberCells, currCell);

                    rowCells = $.merge(rowCells, cellsWithSameValue);
                    lastFoundCell = currCell;
                    currCell = node_before(currCell);
                }
            }

            return rowCells;
        }

        function node_before(sib){
            for(;sib=sib.previousSibling;) {
                if (!is_ignorable(sib)) {
                    return sib;
                }
            }

            return null;
        }

        function is_ignorable(nod){
            return 8==nod.nodeType || 3==nod.nodeType && is_all_ws(nod);
        }

        function is_all_ws(nod){
            return !/[^\t\n\r ]/.test(nod.data);
        }

        function getCellsByIndexAndValue(allCells,fieldIndex,value,isDateTime){
            function exactTextMatch(){
                return isDateTime?_.isEqual(Date.parseDate(this.getAttribute("val")),value):this.getAttribute("val") === value;
            }

            var matchingCells = allCells.filter(exactTextMatch).filter("[fIdx = "+fieldIndex+"], [fDimensionIdx = "+fieldIndex+"]");

            return matchingCells;
        }

        function getCellsFromSameFieldWithSameTextValue(allCells,cell){
            var value = $(cell).attr("val"),fieldIndex=parseInt(cell.getAttribute("fIdx"));

            return getCellsByIndexAndValue(allCells,fieldIndex,value);
        }

        // given a cell, gets the dimension member column header cells for that cell, for all column dimensions. (i.e for a value cell of column 'ipod nano -> apple' returns 'ipod nano', 'apple'
        // cell can be either a value cell, or a dimension member header cell.
        function getColumnCells(cell, headersContainer, orderedLowermostCells, isLowermostHeaderCell) {
            var columnCells = []
            var headerCell = cell;
            if (!isLowermostHeaderCell) {
                var indexAmongstValueCells = $(cell).parent().children('td.p-value').index(cell);
                headerCell = _.reject(orderedLowermostCells, function(c){
                    return c.className.indexOf('p-dim-head') >= 0;
                })[indexAmongstValueCells];
            }

            // find appropriate header cells. the lowermost row has the same columns as the values row; therefore- the cell index would be the same.
            var allDimensionHeaderCells = $(headersContainer).find('td.p-dim-member-head');

            if (headerCell.className.indexOf('p-dim-member-head') < 0) {
                // not a dimension member (a value header)- retrieve the correct dimension header using the measure path, if available
                headerCell = allDimensionHeaderCells.filter('[measurePath = "' + headerCell.getAttribute('measurePath') + '"]')[0] || headerCell;
            }

            var allCellsWithSameValue = getCellsFromSameFieldWithSameTextValue(allDimensionHeaderCells, headerCell);
            $.merge(columnCells, allCellsWithSameValue);

            var measurePath = headerCell.getAttribute('measurePath');
            if (!measurePath)
                return columnCells;

            var lastIndexOfComma = measurePath.lastIndexOf('","');
            while (lastIndexOfComma > 0) {
                measurePath = measurePath.slice(0, lastIndexOfComma + 1) + '}';
                headerCell = headersContainer.find('thead td[measurePath = "' + measurePath + '"]')[0];

                var allCellsWithSameValue = getCellsFromSameFieldWithSameTextValue(allDimensionHeaderCells, headerCell);
                $.merge(columnCells, allCellsWithSameValue);

                lastIndexOfComma = measurePath.lastIndexOf('","');
            }
            return columnCells;
        }

        var lowermostCells = getLowermostHeaderCellsInOrder($('.p-table'));
        var colCells = getColumnCells(cell, $('.p-table'), lowermostCells);
        var rowCells = getRowCells(cell, $('.p-table'));
        var filters = [];

        $.merge(rowCells, colCells);

        angular.forEach(rowCells, function (memberCell) {
            var fieldIndex;
            var items;

            if (memberCell.className.indexOf('p-measure-head') >= 0){
                fieldIndex = parseInt(memberCell.getAttribute('fDimensionIdx'));
                items = widget.metadata.panel('columns').items;
            }
            else if (memberCell.className.indexOf('p-dim-member-head') == 0){
                fieldIndex = parseInt(memberCell.getAttribute('fIdx'));
                items = widget.metadata.panel('columns').items;
            }
            else {
                fieldIndex = parseInt(memberCell.getAttribute('fIdx'));
                items = widget.metadata.panel('rows').items;
            }

            var filter = createFilter(items, fieldIndex, $(memberCell).attr('val'));

            filters.push(filter);
        });

        return filters
    }

