//hide drilled folders for none owners
navver.directive('listItemHolder', [
	function () {
		return {
			restrict: 'C',
			link: function link($scope, lmnt, attrs) {
				if ($scope.listItem.title.match("^_drill_")) {
					if (prism.user._id != $scope.listItem.owner){
						$(lmnt).hide();
					}
				}
			}
		}
	}
]);

prism.removeHandler = function(eventName, handler){

    var eventdef = $$get(this.$ngscope.$$listeners[eventName]);
    if (!defined(eventdef)) {

        throw "the " + eventName + " event is undefined";
    }

    // not registered
    var index = eventdef.indexOf(handler);
    if (index < 0) {

        return;
    }

    // removing handler
    eventdef.splice(index, 1);

    return this;

};

prism.run([function() {


	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
	});

	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
        var allowedChartTypesToDrillFrom = ["chart/pie","chart/line","chart/area", "chart/bar", "chart/column"];
		// hooking to pivot ready/destroyed events
		if (args.widget.type === "pivot" && defined(args, "widget.options.drillTarget")) {
			args.widget.on("ready", onPivotReady);
			args.widget.on("destroyed", onWidgetDestroyed);
		}
        else if (allowedChartTypesToDrillFrom.indexOf(args.widget.type) >-1 && defined(args, "widget.options.drillTarget")) {
			if(prism.$ngscope.$$listeners.beforemenu.indexOf(onBeforeMenuDataPoint) == -1){
                prism.on("beforemenu", onBeforeMenuDataPoint);
                args.widget.on("destroyed",removeOnBeforeMenuDataPoint)
            }
			//args.widget.on("destroyed", removeOnBeforeMenuDataPoint);
		}
	}

	// unregistering widget events
	function onWidgetDestroyed(widget, args) {
		widget.off("ready", onPivotReady);
		widget.off("destroyed", onWidgetDestroyed);
	}

    function removeOnBeforeMenuDataPoint(widget, args) {

        prism.removeHandler("beforemenu", onBeforeMenuDataPoint);

    }

    function onBeforeMenuDataPoint(e, args){

        if (args.settings.name != "datapoint" || !defined(args, "settings.widget.options.drillTarget")) {

            return;
        }
        var widget = args.settings.widget;
        args.settings.items.push({ type: "separator" });
        args.settings.items.push({

            caption: "Drill to dashboard..",
            closing: true,
            execute: function () {

                var filters = args.settings.jaql || [];

                widget.dashboard.filters.flatten().forEach(function (f) { filters.push({ jaql: $$.object.clone(f.jaql, true) }); }); // get dashboard filters
                widget.metadata.filters().forEach(function (f) { filters.push({ jaql: $$.object.clone(f.jaql, true) }); }); // get widget filters

                //set dashboard panes options - true is default
                var rightPane = true; // filterpane
                var leftPane = true; //dashboards pane
                var toolbar = true; // toolbar row
                var header = true; // header row
                var volatile = true; //if volatile=true -> do not save changes to mongo

                var dashboardOptionsStr = "";
                dashboardOptionsStr += rightPane ? "" : "&r="+ rightPane;
                dashboardOptionsStr += leftPane ? "" : "&l="+ leftPane;
                dashboardOptionsStr += header ? "" : "&h="+ header;
                dashboardOptionsStr += toolbar ? "" : "&t="+ toolbar;
                dashboardOptionsStr += "&volatile=" + volatile;

                window.open("/app/main#/dashboards/" + widget.options.drillTarget.oid + "?filter=" + encodeURIComponent(JSON.stringify(filters)) + dashboardOptionsStr, "_blank");
            }
        });

    }

	// create dashboards menu
	prism.on("beforemenu", function (e, args) {

		try {

            if(e.currentScope.appstate != "widget"){
                return;
            }
            var allowedChartTypesToDrillFrom = ["chart/pie","chart/line","chart/area", "chart/bar", "chart/column"];
			if (prism.activeWidget.type != "pivot" && allowedChartTypesToDrillFrom.indexOf(e.currentScope.widget.type) == -1){
				return;
			}

			if (!$(args.ui.target).hasClass('wet-menu')){
				return;
			}

			//create dashboards menu
				//save 'drill to dashboard' id
				function drillTo() {
					if (this.checked){
						prism.activeWidget.options.drillTarget = null;
						this.checked = false;
					}
					else {
						prism.activeWidget.options.drillTarget = {oid: this.oid, caption: this.caption};
					}
				}

				//get all dashboards with the same data source except current dashboard
				function getDashboards(currDS, drillTarget){
					var drillMenuItems = [];

					$.ajax({
						type: "GET",
						url: "/api/dashboards",
						async : false
					})
						.done(function( data ) {
							data.forEach(function (item){
								if (item.oid != currDS && item.datasource.id == prism.activeWidget.dashboard.datasource.id && item.title.match("^_drill_")) {
									drillMenuItems.push({
										caption: item.title,
										type: "option",
										execute: drillTo,
										checked: (defined(drillTarget) &&  item.oid == drillTarget.oid) ? true: false,
										oid: item.oid,
										classes: 'drillDSselected'
									});
								}
							});
						});

					return drillMenuItems;
				}

				//create dashboard menu
				var ds = getDashboards(prism.activeWidget.dashboard.oid, prism.activeWidget.options.drillTarget);

				if (ds.length) {
					args.settings.items.push({type: "separator"});
					args.settings.items.push({
						caption: "Drill-To Dashboard",
						items: ds
					});
				}

		}
		catch (e) {
			console.log(e);
		}
	});

	//create dashboard links with filters for each pivot cell
	function onPivotReady(widget, args) {
		try {
            setTimeout(function(){
                widget.refreshing = true;
                angular.element($('body')).scope().$apply();

                //get all pivot cells
                var cells = $("td.p-value");
                var dsfilters = [];

                $(widget.dashboard.filters.$$items).each(function (){
                    if (this.levels) {
                        $(this.levels).each(function (){
                            var item = {jaql:{}};
                            var tmpitem = $(this)[0];

                            item.jaql.datatype = tmpitem.datatype;
                            item.jaql.dim = tmpitem.dim;
                            item.jaql.filter = tmpitem.filter;

                            if (tmpitem.level) {
                                item.jaql.level = tmpitem.level;
                            }

                            console.log(item);

                            dsfilters.push(item);
                        });
                    }
                    else{
                        var item = {jaql:{}};
                        var tmpitem = this.jaql;

                        item.jaql.datatype = tmpitem.datatype;
                        item.jaql.dim = tmpitem.dim;
                        item.jaql.filter = tmpitem.filter;

                        if (tmpitem.level) {
                            item.jaql.level = tmpitem.level;
                        }

                        console.log(item);

                        dsfilters.push(item);
                    }
                });

                cells.each(function () {
                    var filters = $.merge(getCellFilters(this, widget), dsfilters);

                    //create drill to dashboard link with calculated filter
                    var link = "javascript:window.open('/app/main#/dashboards/" + widget.options.drillTarget.oid + "?filter=" + encodeURIComponent(JSON.stringify(filters)) + "');";

                    if ($(this).find('span').length) {
                        $(this).find('span').html('<a href="' + link + '">' + $(this).find('span').html() + '</a>');
                    }
                    else {
                        $(this).find('div').html('<a href="' + link + '">' + $(this).find('div').text() + '</a>');
                    }
                });

                widget.refreshing = false;
                angular.element($('body')).scope().$apply();
            },80);

		}
		catch (e){
			widget.refreshing = false;
			angular.element($('body')).scope().$apply();
			console.log(e);
		}
	}
}]);