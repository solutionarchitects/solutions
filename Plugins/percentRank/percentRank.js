//add 'Percent Rank' menu item to 'Quick Functions' menu
prism.on("beforemenu", function (e, args) {
	try {
		if (!prism.$ngscope.widgetType.match("^chart")){
			return;
		}

		//find values menu
		if (!$(args.ui.target.hasClass('ew-i-header-host'))){
			return;
		}

		//add 'Percent Rank' menu item to 'Quick Functions' menu
		args.settings.items[11].items.push({caption: 'Percent Rank',
			type: "check",
			execute: percentRank,
			checked: defined(angular.element(args.ui.target).scope().item.percentRank) ? angular.element(args.ui.target).scope().item.percentRank : false
		});

		function percentRank() {
			this.checked = !this.checked;
			angular.element(args.ui.target).scope().item.percentRank = this.checked;

			if (this.checked){
				angular.element(args.ui.target).scope().item.format.mask = {type : "percent"};
			}

			prism.$ngscope.widget.refresh();
		}
	}
	catch (e) {
		console.log(e);
	}
});

prism.run([function() {
	// Registering widget events
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
	});

	prism.on("widgetloaded", function (e, args) {
		if (args.widget.type.match("^chart")){
			args.widget.on("processresult", onChartProcessResult);
			args.widget.on("destroyed", onWidgetDestroyed);
		}
	});

	function onWidgetInitialized(dashboard, args) {
		if (args.widget.type.match("^chart")){
			args.widget.on("processresult", onChartProcessResult);
		}
	}

	// Registering widget events for new widget created in Edit Panel when switching between widget types
	function onWidgetDestroyed(widget, args) {
		prism.$ngscope.$watch('widget.type',
			function(newValue, oldValue){
				if (newValue && prism.$ngscope.widget.type.match("^chart")){
					prism.$ngscope.widget.on("processresult", onChartProcessResult);
					prism.$ngscope.widget.on("destroyed", onWidgetDestroyed);
				}
			});
	}

	//calculate percent rank
	function onChartProcessResult(widget, args){
		try{
			for (var k = 0; k < args.rawResult.metadata.length; k++) {
				if (defined(args.rawResult.metadata[k].percentRank) && (args.rawResult.metadata[k].percentRank)) {
					var values = args.rawResult.values;

					//copy values array to a temp array - [0]: value, [1]: original index, [2]: value occurrences, [3]: number of values less then current value
					var test_with_index = [];

					for (var i = 0; i < values.length; i++) {
						test_with_index.push([values[i][k].data, i, 0, 0]);
					}

					test_with_index.sort(function (a, b) {
						return a[0] - b[0];
					});

					//calculate value occurrences and number of values less then current value
					var currValueIndexes = [];
					var prevValue = null;
					var numValuesLessThenCurrent = -1;

					for (var j = 0; j < test_with_index.length; j++) {
						var currValue = test_with_index[j][0];

						//new value or last item
						if (prevValue != currValue) {
							//set value occurrences for items of the same value and number of values less then current value
							for (var i = 0; i < currValueIndexes.length; i++) {
								test_with_index[currValueIndexes[i]][2] = currValueIndexes.length - 1;
								test_with_index[currValueIndexes[i]][3] = numValuesLessThenCurrent;
							}

							prevValue = test_with_index[j][0];
							currValueIndexes = [j];
							numValuesLessThenCurrent += 1;
						}
						else {
							currValueIndexes.push(j);
						}

						//set value occurrences for last item  and number of values less then last value
						if (j == test_with_index.length - 1) {
							for (var i = 0; i < currValueIndexes.length; i++) {
								test_with_index[currValueIndexes[i]][2] = currValueIndexes.length - 1;
								test_with_index[currValueIndexes[i]][3] = numValuesLessThenCurrent;
							}
						}
					}

					//calculate and set percent rank
					for (var i = 0; i < test_with_index.length; i++) {
						var valuesLessThenCurrent = test_with_index[i][3];
						var occurrences = test_with_index[i][2];
						var valuesCount = test_with_index.length - 1;
						var percentRank = (valuesLessThenCurrent + 0.5 * occurrences) / valuesCount;
						var originalIndex = test_with_index[i][1];

						values[originalIndex][k].data = percentRank;
						values[originalIndex][k].text = percentRank.toString();
					}

					//copy calculated data to series array
					var findArr = $.grep(args.result.series, function (e) {
 						return e.name == args.rawResult.headers[k];
 					});

					for (var i = 0; i < findArr[0].data.length; i++) {
						findArr[0].data[i].y = args.rawResult.values[i][k].data;
					}
				}
			}
		}
		catch (e){
			console.log(e);
		}
	}
}]);