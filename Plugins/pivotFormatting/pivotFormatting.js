prism.run([function() {
	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
	});

	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
		// hooking to pivot ready/destroyed events
		if (args.widget.type === "pivot") {
			args.widget.on("ready", onPivotReady);
			args.widget.on("destroyed", onWidgetDestroyed);
		}
	}

	// unregistering widget events
	function onWidgetDestroyed(widget, args) {
		widget.off("ready", onPivotReady);
		widget.off("destroyed", onWidgetDestroyed);
	}

	//format previous pivot cell according to current cell
	function formatCell(cell){
		if (30 > cell.attr('val')){
			cell.prev().addClass('cell');
		}
	}

	//create formatting
	function onPivotReady(widget, args) {
		try {
			//get all column cells and format previous ones 
			var cells = $('#pivot_').find('td.p-value[fidx=2]');

			cells.each(function () {
				formatCell($(this));
			});

			//hide column
			$('#pivot_').find('col[fidx=2]').hide();

			if ($('pivot').attr('orgWidth')){
				var width = $('pivot').attr('orgWidth');
			}
			else{
				var width = $('pivot').width();
				$('pivot').attr('orgWidth', width );
			}

			width -= $('#pivot_').find('col[fidx=2]').width();
			$('pivot').width(width);
		}
		catch (e){
			console.log(e);
		}
	}
}]);