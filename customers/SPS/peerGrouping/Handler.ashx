﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Script.Serialization;
using System.IO;
using Prism.Web.AppServices.Http.Session;
using System.Collections.Generic;
using JWT;
using System.Configuration;

/// <summary>
///  Returns all data including unauthorized data.
///  If data security is defined for the current user, Sets the value of request parameter 'dim' (break by/color dimension) of unauthorized data to DEFAULT_VALUE.
///  Returns empty data set in case the count of distinct THRESHOLD_TABLE.THRESHOLD_COLUMN values is smaller than VALUES_THRESHOLD.
/// </summary>
public class Handler : IHttpHandler {
    private string email;
    private string password;
    private string restAPIToken;
    private string dafaultValue;
    private string thresholdTable;
    private string ThresholdColumn;
    private int threshold;
    private string dimesnion;
    private string baseUrl;
    private string requestJson;
    private Dictionary<string, object> datasource;
    private string token;
    
    /// <summary>
    ///  Enables processing of HTTP Web requests by a custom HttpHandler that implements
    ///  the System.Web.IHttpHandler interface.
    /// </summary>
    /// <param name="context">
    ///  An System.Web.HttpContext object that provides references to the intrinsic
    ///  server objects (for example, Request, Response, Session, and Server) used
    ///  to service HTTP requests.
    /// </param>
    public void ProcessRequest (HttpContext context) 
    {
        try
        {
            //get config parameters
            email = ConfigurationManager.AppSettings["EMAIL"];
            password = ConfigurationManager.AppSettings["PASSWORD"];
            restAPIToken = ConfigurationManager.AppSettings["REST_API_TOKEN"];
            dafaultValue = ConfigurationManager.AppSettings["DEFAULT_VALUE"];
            thresholdTable = ConfigurationManager.AppSettings["THRESHOLD_TABLE"];
            ThresholdColumn = ConfigurationManager.AppSettings["THRESHOLD_COLUMN"];
            threshold = int.Parse(ConfigurationManager.AppSettings["VALUES_THRESHOLD"]);

            //Get the current user
            PrismAuthenticatedIdentity userIdentity = (PrismAuthenticatedIdentity)context.User.Identity;

            //get dimension name for 'hiding'
            dimesnion = context.Request.Params["dim"];
            
            //calculate base URL
            baseUrl = context.Request.Url.Scheme + "://" + context.Request.Url.Authority + context.Request.ApplicationPath.TrimEnd('/') + "/"; 

            //get request data
            context.Request.InputStream.Position = 0; 
            StreamReader inputStream = new StreamReader(context.Request.InputStream);
            requestJson = inputStream.ReadToEnd();

            //get data source
            JavaScriptSerializer serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue};
            var jsonDict = serializer.Deserialize<dynamic>(requestJson);
            datasource = jsonDict["datasource"];
            
            //create token 
            token = createToken();

            string response = string.Empty;

            //check threshold
            if (checkThreshold(userIdentity.UserID))
            {
                //claculate new data
                string authorizedData = getAuthorizedData(context.Request.Headers["Cookie"]);
                string allData = getAllData();
                response = hideMergeData(authorizedData, allData, userIdentity.UserID);
            }
            else //send empty response
            {
                response = "{\"headers\":[],\"metadata\":[],\"values\":[]}";
            }
           
            context.Response.Write(response);
        }
        catch (Exception)
        {
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
        }
       
    }
    
    /// <summary>
    ///  Create Admin User Token
    /// </summary>
    /// <returns>Token</returns>
    private string createToken(){
        TimeSpan t = (DateTime.UtcNow - new DateTime(1970, 1, 1));
        int timestamp = (int)t.TotalSeconds;
        Dictionary<string, object> payload = new Dictionary<string, object>() {
            { "iat", timestamp },
            { "email",  email},
		    { "password",  password}};
        string token = JsonWebToken.Encode(payload, restAPIToken, JwtHashAlgorithm.HS256);
        
        return token;
    }

    /// <summary>
    /// Hide unauthorized data and merge the hidden data with the user data
    /// </summary>
    /// <param name="userData">authorized data</param>
    /// <param name="allData">unauthorized data</param>
    /// <param name="userId">user id</param>
    /// <returns>Date</returns>
    private string hideMergeData(string userData, string allData, string userId)
    {
        string dataStr = "";
        JavaScriptSerializer serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
        var allDataDict = serializer.Deserialize<dynamic>(allData);
        int index = Array.IndexOf(allDataDict["headers"], dimesnion);
        var userDataDict = serializer.Deserialize<dynamic>(userData);
        List<string> userDimValues = new List<string>();
        
        //user Values as list
        List<object> userValues = new List<object>();

        foreach (var item in  userDataDict["values"])
        {
            userValues.Add(item);
        }

        //create a distinct list of user dimension values
        for (int i = 0; i < userDataDict["values"].Length; i++)
        {
            if (!userDimValues.Contains(userDataDict["values"][i][index]["data"]))
            {
                userDimValues.Add(userDataDict["values"][i][index]["data"]);
            }
        }
         
        //hide and merge unauthorized data   
        for (int i = 0; i < allDataDict["values"].Length; i++)
        {  
            var e = allDataDict["values"][i];
                
            //check if the value is unauthorized  
            if (!userDimValues.Contains(e[index]["data"]))
            {
                //set the value of dimesnion to dafaultValue
                e[index]["data"] = dafaultValue;
                e[index]["text"] = dafaultValue;
                
                //add item to user values
                userValues.Add(e);
            }
        }

        userDataDict["values"] = userValues.ToArray();
        dataStr = serializer.Serialize(userDataDict);

        return dataStr;
    }

    /// <summary>
    /// Get data security or the current user
    /// </summary>
    /// <param name="userId">user id</param>
    /// <param name="thresholdJaql">threshold Jaql</param>
    /// <returns>bool</returns>
    private bool setUserDataSecurityThresholdJaql(string userId, Dictionary<string, Dictionary<string, object>> thresholdJaql)
    {
        bool hasDataSecurity = false;
        
        //create http request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl + "/api/elasticubes/" + datasource["address"] + "/" + datasource["title"] + "/" + userId + "/datasecurity");
        request.Method = "GET";
        request.ContentType = "application/json; charset=utf-8";
        request.Headers["X-API-KEY"] = token;

        //get response
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        JavaScriptSerializer serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
        string jsonStr = reader.ReadToEnd();
        var dataSecurity = serializer.Deserialize<dynamic>(jsonStr);

        //check if user has data security
        for (int i = 0; i < dataSecurity.Length; i++)
        {
            var arr = dataSecurity[i];

            if (arr["shares"][0]["type"] == "user" && arr["members"].Length > 0)
            {
                hasDataSecurity = true;
                
                //add data security memners to jaql object
                thresholdJaql.Add("scope", new Dictionary<string, object>(){
                    {"dim", "[" + arr["table"] + "." + arr["column"] + "]"}, 
                    {"filter", new Dictionary<string, object>(){
                        {"exclude", new Dictionary<string, object>(){
                            {"members", arr["members"]}
                        }}
                    }}
                });
            }
        }

        return hasDataSecurity;
    }

    /// <summary>
    /// Get authorized data
    /// </summary>
    /// <param name="cookie"></param>
    private string getAuthorizedData(string cookie)
    {
        //create http request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl + "/api/elasticubes/" + datasource["title"] + "/jaql");
        
        request.Method = "POST";
        request.ContentType = "application/json; charset=utf-8";
        request.Headers.Add("Cookie", cookie);

        StreamWriter streamWriter = new StreamWriter(request.GetRequestStream());

        streamWriter.Write(requestJson);
        streamWriter.Flush(); 

        //get response
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        string userData = reader.ReadToEnd();

        return userData;
    }

    /// <summary>
    /// Get all data including unauthorized data
    /// </summary>
    /// <returns></returns>
    private string getAllData()
    {
        string data = "";

        //create http request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl + "/api/elasticubes/" + datasource["title"] + "/jaql");

        request.Method = "POST";
        request.ContentType = "application/json; charset=utf-8";
        request.Headers["X-API-KEY"] = token;

        StreamWriter streamWriter = new StreamWriter(request.GetRequestStream());

        streamWriter.Write(requestJson);
        streamWriter.Flush();

        //get response
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        StreamReader reader = new StreamReader(response.GetResponseStream());
        data = reader.ReadToEnd();

        return data;
    }

    /// <summary>
    /// check threshold
    /// </summary>
    /// <returns></returns>
    private bool checkThreshold(string userId)
    {
        bool isThresholdExceeded = false;
       
        //create http request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl + "/api/elasticubes/" + datasource["title"] + "/jaql");
        request.Method = "POST";
        request.ContentType = "application/json; charset=utf-8";
        request.Headers["X-API-KEY"] = token;

        //add threshold jaql
        JavaScriptSerializer serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };
        var jsonDict = serializer.Deserialize<dynamic>(requestJson);
        Dictionary<string, object> jaqlObj = new Dictionary<string, object>(jsonDict);
        List<object> jaqls = new List<object>();

        for (int i = 0; i < jsonDict["metadata"].Length; i++)
        {
            if (jsonDict["metadata"][i].ContainsKey("panel") && jsonDict["metadata"][i]["panel"] == "scope")
            {
                jaqls.Add(jsonDict["metadata"][i]);
            }
        }
       
        Dictionary<string, Dictionary<string, object>> thresholdJaql = new Dictionary<string, Dictionary<string, object>> { {"jaql", new Dictionary<string, object> { 
                {"dim", "[" + thresholdTable + "." + ThresholdColumn + "]"}, 
                {"agg", "count"} 
        }}};

        //check if user has data security and set the threshold jaql object
        bool hasDataSecurity = setUserDataSecurityThresholdJaql(userId, thresholdJaql);

        if (!hasDataSecurity)
        {
            isThresholdExceeded = true;
        }
        else
        {
            jaqls.Add(thresholdJaql);
            jaqlObj["metadata"] = jaqls;

            string jaqlsStr = serializer.Serialize(jaqlObj);
            StreamWriter streamWriter = new StreamWriter(request.GetRequestStream());
            streamWriter.Write(jaqlsStr);
            streamWriter.Flush();

            //get response
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string data = reader.ReadToEnd();
            var thresholdData = serializer.Deserialize<dynamic>(data);

            //check threshold
            if (thresholdData["values"].Length > 0 && thresholdData["values"][0]["data"] > threshold)
            {
                isThresholdExceeded = true;
            }
        }

        return isThresholdExceeded;
    }
    
    public bool IsReusable {
        get {
            return false;
        }
    }
}