+************************************************+
* SiSense SPS Scatter Distribution API Plugin  *
+************************************************+

-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
Description:
--  Returns unauthorized data with hidden values. 
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
-- Parameters ----------------------------------------------------------------------
------------------------------------------------------------------------------------
EMAIL 				admin email 
PASSWORD 			admin password 
REST_API_TOKEN 		REST API token 
DEFAULT_VALUE		Unauthorized column new value 
THRESHOLD_TABLE		Table name for the threshold 
THRESHOLD_COLUMN	Column name for the threshold 
VALUES_THRESHOLD	Threshold of unauthorized values to be shown

Returns all data including unauthorized data.
If data security is defined for the current user, Sets the value of request parameter 'dim' (break by/color dimension) of unauthorized data to DEFAULT_VALUE.
Returns empty data set in case the count of distinct THRESHOLD_TABLE.THRESHOLD_COLUMN values is smaller than VALUES_THRESHOLD.








