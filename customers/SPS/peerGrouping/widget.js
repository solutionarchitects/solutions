widget.on("beforequery", function(widget, args){
	args.query.endpoint = "/peerGrouping/Handler.ashx?dim=Company";
});

widget.on('render', function(widget, args){ 
 	var seriesName = "ALL" // the series name we want to change the color.
	var color = "#000000";
	var series = widget.queryResult.series;
	
 	// fine the series
 	var ser = _.find(series, function(ser){
 		return ser.name == seriesName
 	});
	
	if (ser){
		ser.color = color;
		_.each(ser.data, function(e){e.color = color; });
	}
});