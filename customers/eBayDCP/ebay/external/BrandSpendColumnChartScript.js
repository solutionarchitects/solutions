/**
 * Created by atavdi on 12/1/14.
 */

//column chart script!
prism.removeOnContainerClick(widget);
prism.displayErrorNoData(widget, "No Data To Display")

var functionToRun = function(w, ev){
    ev.result.plotOptions.series.stickyTracking= false;
    ev.result.series.splice(0,0,ev.result.series.splice(1,1)[0]);
    ev.result.legend.y = 20;
    ev.result.navigator.enabled = false;
    ev.result.navigator.enabledFromEditor = false;
    if(ev.rawResult.headers[0] == "DSC_MONTH"){
        prism.ebayGlobals.sortMonths(w,ev);
    }
    else{
        prism.ebayGlobals.sortWeeks(w, ev);
    }
}
var panelToFilterBy = {
    panel:'categories',
    index:0
}
var subTypesToFilter = ['pie/classic', 'pie/donut','pie/ring'];

prism.registerFilterOnHover(widget, panelToFilterBy, subTypesToFilter, functionToRun);


