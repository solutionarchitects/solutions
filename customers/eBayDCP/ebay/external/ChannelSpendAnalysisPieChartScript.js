

//pie chart script!
prism.removeOnContainerClick(widget);

widget.on('processresult', function(w,e){

    var colors = ["#8b8bc2", "#ff8380", "#00cee6", "#fbb755", "#64c44d"];
    e.result.series[0].data.sort(function(a,b){return prism.ebayGlobals.sortedChannelTypes[a.name.toLowerCase()] - prism.ebayGlobals.sortedChannelTypes[b.name.toLowerCase()]})
    for(var i = 0 ; i< e.result.series[0].data.length; i++){
        var j = i;
        j = j == colors.length ? 0 : j;
        e.result.series[0].data[i].color = colors[j]
    }

    e.result.plotOptions.pie.innerSize = '70%';

    //draw inner text
    var onLoad = function(ev,chart){
        if(!w.rawQueryResult.values || !w.rawQueryResult.values.length) return // if no data return
        $(this.container).parent().append('<div id="addText"></div>');
        $('#addText').css({
            'position':'absolute',
            'left':'0px',
            'top':'0px',
            'vertical-align':'middle',
            'font-family':'Ubuntu, sans-serif serif',
            'text-align':'center'
        })

        var textX = this.plotLeft + (this.series[0].center[0]);
        var textY = this.plotTop  + (this.series[0].center[1]);


        var upperText =  $$get(w.metadata.panel('filters'),'items.0.jaql.filter.members.0');
        if(!upperText) { upperText = ''}
        else if(upperText.getMonth) { upperText = prism.ebayGlobals.getFormattedDate(upperText) };

        var sum = 0;
        for(var i = w.rawQueryResult.values.length - 1; i>=0;i--){
            sum += w.rawQueryResult.values[i][1].data;
        }

        var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
        span += '<span style="font-size: 15px">'+ upperText +'</span><br>';
        span += '<hr>';
        span += '<span style="font-size: 15px">'+ '$'+prism.ebayGlobals.abbrNum(sum,2) +'</span>';
        span += '</span>';

        $("#addText").append(span);
        span = $('#pieChartInfoText');
        span.css('left', textX + (span.width() * -0.5));
        span.css('top', textY + 29 + (span.height() * -0.5));

        $(".highcharts-legend-item path", this.container)
            .attr("stroke-width", "12")
            .attr("width", "12")
            .attr("d", "M 0 12 L 12 12"),
            $(".highcharts-legend-item rect", this.container)
                .attr("rx", "0")
                .attr("ry", "0")
                .attr("width", "12")
                .attr("height", "12")
    }
    e.result.chart.events.load =onLoad;
})
