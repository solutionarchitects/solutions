widget.on('processresult', function (w, ev) {
    if(window.hovered){
        window.hovered = null;
    }
    var generateCartesianTooltip = function () {
        return '<div class="cartesian-tooltip">' +
            '<div class="cartesian-tooltip-values" ng-repeat="item in model.points">' +
            '<div class="cartesian-tooltip-seriesName"> {{item.seriesName}} </div> ' +
            '<div class="cartesian-tooltip-singleLineValue" style="color:{{item.valueColor}}"> ' +
            '<div class="cartesian-tooltip-value"> {{item.value}} </div> <div class="cartesian-tooltip-percentage" data-ng-show="item.showPercentage"> / {{item.percentage}} </div> </div>' +
            '</div>' + // values
            '<div class="highcharts-tooltip-line-separator"></div>' + // line separator
            '<div class="cartesian-tooltip-xAxis"> <div class="cartesian-tooltip-plotBand" data-ng-show="model.hasTwoXAxis"> {{model.plotBand}},&nbsp </div> ' +
            '<div class="cartesian-tooltip-category">{{model.category}} </div> </div>' + // first line - xAxis details
            '</div>';

    };

    var hoverOverMarker = function (ev) {
        window.hovered = w._id;
        console.log('hovered');
        var member = this.category;

        var jaql = $.extend(w.metadata.panel('categories').items[0].jaql,{});
        jaql.filter = {};
        jaql.filter.members = [member];
        var filter = {
            jaql: jaql
        }
        var widget = _.find(w.dashboard.widgets.$$widgets, function(w){return (w.subtype == 'pie/classic') || (w.subtype =='pie/donut') || (w.subtype =='pie/ring')})
        if(!widget) return;
        var filters = widget.metadata.panel('filters').items;
        while(filters.length){
            filters.pop();
        }
        filters.push(filter);

        widget.refresh();


        console.log('hovered');
        var $dom = w.$dom;

        var familyType = 'cartesian';
        var template = generateCartesianTooltip();
        var tipper;
        var tipscope = {};
        tipper = $dom.tip({scope: tipscope, template: template}, {css: "charts", placement: {place: "b", anchor: "l"}, initialShowDelay: 5, betweenShowDelay: 10})

        tipscope.category = this.category;
        var plotBands = $$get(this, "series.xAxis.options.plotBands"), numOfPlotBands = $$get(plotBands, "length");
        if (numOfPlotBands > 0) {
            tipscope.hasTwoXAxis = !0;
            for (var i = 0; numOfPlotBands > i; i++)if (this.x > plotBands[i].from && this.x < plotBands[i].to) {
                tipscope.plotBand = $$get(plotBands[i], "label.text");
                break
            }
        } else tipscope.hasTwoXAxis = !1;
        tipscope.points = [];
        var i, point, mask, series = $$get(this, "series.chart.series"), stackTotal = this.stackTotal, point = {}, stackTotal = this.stackTotal, mask = $$get(this, "series.options.mask");
        point.seriesName = $$get(this, "series.options.onlyMeasuresInSeries") === !0 ? series[this.x].name : this.series.name, point.value = defined(mask) ? mask(this.y) : this.y,
            point.valueColor = defined(this.color) ? this.color : this.series.color,
            point.showPercentage = defined(stackTotal) && 0 !== stackTotal,
            point.showPercentage === !0 && (point.percentage = Math.round(this.y / stackTotal * 100).toFixed(0) + "%"), tipscope.points.push(point);
        var chartOffset = $(this.series.chart.renderTo).offset();
        "column" === this.series.type || "bar" === this.series.type ? (tipscope.x = $$get(ev, "currentTarget.tooltipPos.0") || $$get(document, "mousePosition.x") || $$get(event, "clientX"),
            tipscope.y = $$get(ev, "currentTarget.tooltipPos.1") || $$get(document, "mousePosition.y") || $$get(event, "clientY")) : (tipscope.x = chartOffset.left + this.series.chart.plotBox.x + this.plotX,
            tipscope.y = chartOffset.top + this.series.chart.plotBox.y + this.plotY);

        tipper.activate({x: tipscope.x, y: tipscope.y, space: 0});
        this.series.chart.sisenseTipper = {familyType: familyType, type: this.series.type, tipper: tipper, status: "on"};


    }

    var mouseOut = function(ev){
        window.hovered = null;
        var widget = w;
        var widget = _.find(w.dashboard.widgets.$$widgets, function(w){return  (w.subtype == 'pie/classic') || (w.subtype =='pie/donut') || (w.subtype =='pie/ring')})
        if(!widget) return;
        var filters = widget.metadata.panel('filters').items;
        while(filters.length){
            filters.pop();
        }
        widget.refresh();
    }

    ev.result.plotOptions.series.point.events.mouseOver = hoverOverMarker;
    ev.result.plotOptions.series.point.events.mouseOut = mouseOut;
})