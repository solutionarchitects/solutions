//column chart script!
prism.removeOnContainerClick(widget);
prism.displayErrorNoData(widget, 'No Data To Display');

var functionToRun = function(w, ev){
    ev.result.legend.width = 500;
    ev.result.legend.x = 180;
    ev.result.legend.y = 20;
    ev.result.navigator.enabled = false;
    ev.result.navigator.enabledFromEditor = false;
    if(ev.rawResult.headers[0] == "DSC_MONTH"){
        prism.ebayGlobals.sortMonths(w,ev);
    }
    else{
        prism.ebayGlobals.sortWeeks(w, ev);
    }

}
prism.registerFilterOnHover(widget, 'x-axis',['pie/classic', 'pie/donut','pie/ring'],functionToRun);