/**
 * Created by odeckelbaum on 11/4/14.
 */
/*
 Welcome to your Widget's Script.

 - You can access your Widget via the 'widget' variable name.
 - You can access your Widget's DOM via the 'element' variable name (undefined until DOM creation).
 - You can access your Dashboard by accessing the 'dashboard' variable name.

 - For a complete API reference for Widgets and Dashboards go here: https://docs.google.com/document/d/1nQBZtWAdNFAd9nBhPWGVT3qOMS4Qm0PzBZVIzz5DfE8/
 */

/*
 Welcome to your Widget's Script.

 - You can access your Widget via the 'widget' variable name.
 - You can access your Widget's DOM via the 'element' variable name (undefined until DOM creation).
 - You can access your Dashboard by accessing the 'dashboard' variable name.

 - For a complete API reference for Widgets and Dashboards go here: https://docs.google.com/document/d/1nQBZtWAdNFAd9nBhPWGVT3qOMS4Qm0PzBZVIzz5DfE8/
 */


//chart lines script!


widget.on('processresult', function(w,e){

    e.result.legend.x = 175;
    var colors = ["#8b8bc2", "#ff8380", "#00cee6", "#fbb755", "#64c44d"];
    if(e.rawResult.headers[0] == "DSC_MONTH"){
        var months =
        {
            "january" : 0,
            "february" : 1,
            "march" : 2,
            "april" : 3,
            "may" : 4,
            "june" : 5,
            "july" : 6,
            "august" : 7,
            "september" : 8,
            "october" : 9,
            "november" : 10,
            "december" : 11
        };

        var categories = e.result.xAxis.categories;
        var series = e.result.series
        var sortedCategories = categories.slice(0);
        sortedCategories.sort(function(a,b){ return months[a.toLowerCase()] - months[b.toLowerCase()] });


        for(var i = 0;i<series.length; i++){
            var sortableArray = [];
            var data = series[i].data;
            for(var j = 0;j < data.length; j++){
                var obj = {
                    cat: categories[j],
                    data: data[j],
                    index:months[categories[j].toLowerCase()]
                }
                if(data[j].selectionData){
                    (data[j].selectionData[0] = sortedCategories[j]);
                }
                sortableArray.push(obj);

            }

            var sorted = sortableArray.sort(function(a,b){return a.index - b.index});
            series[i].data =  sorted.map(function(d){ return d.data});
        }


        e.result.xAxis.categories = sortedCategories;

    }
    else{

        var weeks = {};
        var numberOfWeeks = 53;
        for(var i = 0; i < numberOfWeeks; i++){
            weeks['w'+(i+1)] = i;
        }

        var categories = e.result.xAxis.categories;
        var series = e.result.series
        var sortedCategories = categories.slice(0);
        sortedCategories.sort(function(a,b){ return weeks[a.toLowerCase()] - weeks[b.toLowerCase()] });


        for(var i = 0;i < series.length; i++){
            var sortableArray = [];
            var data = series[i].data;
            for(var j = 0;j < data.length; j++){
                var obj = {
                    cat: categories[j],
                    data: data[j],
                    index:weeks[categories[j].toLowerCase()]
                }
                if(data[j].selectionData){
                    (data[j].selectionData[0] = sortedCategories[j]);
                }
                sortableArray.push(obj);

            }

            var sorted = sortableArray.sort(function(a,b){return a.index - b.index});
            series[i].data =  sorted.map(function(d){ return d.data});
        }

        console.log(series);
        e.result.xAxis.categories = sortedCategories;
    }
    for(var i = 0 ; i< series.length; i++){
        var j = i;
        j = j == colors.length ? 0 : j;
        series[i].color = colors[j]
    }
})


widget.on('processresult', function (w, ev) {


    var generateCartesianTooltip = function () {
        return '<div class="cartesian-tooltip">' +
            '<div class="cartesian-tooltip-values" ng-repeat="item in model.points">' +
            '<div class="cartesian-tooltip-seriesName"> {{item.seriesName}} </div> ' +
            '<div class="cartesian-tooltip-singleLineValue" style="color:{{item.valueColor}}"> ' +
            '<div class="cartesian-tooltip-value"> {{item.value}} </div> <div class="cartesian-tooltip-percentage" data-ng-show="item.showPercentage"> / {{item.percentage}} </div> </div>' +
            '</div>' + // values
            '<div class="highcharts-tooltip-line-separator"></div>' + // line separator
            '<div class="cartesian-tooltip-xAxis"> <div class="cartesian-tooltip-plotBand" data-ng-show="model.hasTwoXAxis"> {{model.plotBand}},&nbsp </div> ' +
            '<div class="cartesian-tooltip-category">{{model.category}} </div> </div>' + // first line - xAxis details
            '</div>';

    };

    var hoverOverMarker = function (ev) {
        console.log('hovered');
        var $dom = w.$dom;

        var familyType = 'cartesian';
        var template = generateCartesianTooltip();
        var tipper;
        var tipscope = {};
        switch (tipper || (tipper = $dom.tip({scope: tipscope, template: template}, {css: "charts", placement: {place: "b", anchor: "l"}, initialShowDelay: 0, betweenShowDelay: 0})), familyType) {
            case"pie":
                var maxOthersContent = 5, contentLength = $$get(this, "content.length"), val = this.y * (this.isNegative === !0 ? -1 : 1);
                if (tipscope.seriesName = this.series.options.name, tipscope.noSeriesName = "" === tipscope.seriesName ? !0 : !1, tipscope.seriesColor = this.color, tipscope.category = this.name,
                    tipscope.isOthersSlice = "Others" === this.name ? !0 : !1, tipscope.spaceUnderValues = tipscope.isOthersSlice ? 6 : 0,
                    tipscope.moreInOthers = defined(contentLength) ? contentLength - maxOthersContent : 0,
                    tipscope.hasMoreInOthers = tipscope.moreInOthers > 0,
                    tipscope.othersDetails = contentLength ? this.content.slice(0, maxOthersContent) : null, tipscope.value = this.mask ? this.mask(val) : numberMask(val),
                    tipscope.percent = Math.round(this.percentage), $$get(tipscope, "othersDetails.length") > 0) {
                    var mask = this.mask || $filter("number");
                    _.each(tipscope.othersDetails, function (item) {
                        angular.isNumber(item.y) && (item.y = mask(item.y))
                    })
                }
                tipscope.x = $$get(ev, "currentTarget.tooltipPos.0") || $$get(document, "mousePosition.x") || $$get(event, "clientX"), tipscope.y = $$get(ev, "currentTarget.tooltipPos.1") || $$get(document, "mousePosition.y") || $$get(event, "clientY");
                break;
            case"cartesian":
            case"polar":
                tipscope.category = this.category;
                var plotBands = $$get(this, "series.xAxis.options.plotBands"), numOfPlotBands = $$get(plotBands, "length");
                if (numOfPlotBands > 0) {
                    tipscope.hasTwoXAxis = !0;
                    for (var i = 0; numOfPlotBands > i; i++)if (this.x > plotBands[i].from && this.x < plotBands[i].to) {
                        tipscope.plotBand = $$get(plotBands[i], "label.text");
                        break
                    }
                } else tipscope.hasTwoXAxis = !1;
                tipscope.points = [];
                var i, point, mask, series = $$get(this, "series.chart.series"), stackTotal = this.stackTotal, point = {}, stackTotal = this.stackTotal, mask = $$get(this, "series.options.mask");
                point.seriesName = $$get(this, "series.options.onlyMeasuresInSeries") === !0 ? series[this.x].name : this.series.name, point.value = defined(mask) ? mask(this.y) : this.y,
                    point.valueColor = defined(this.color) ? this.color : this.series.color,
                    point.showPercentage = defined(stackTotal) && 0 !== stackTotal,
                    point.showPercentage === !0 && (point.percentage = Math.round(this.y / stackTotal * 100).toFixed(0) + "%"), tipscope.points.push(point);
                var chartOffset = $(this.series.chart.renderTo).offset();
                "column" === this.series.type || "bar" === this.series.type ? (tipscope.x = $$get(ev, "currentTarget.tooltipPos.0") || $$get(document, "mousePosition.x") || $$get(event, "clientX"),
                    tipscope.y = $$get(ev, "currentTarget.tooltipPos.1") || $$get(document, "mousePosition.y") || $$get(event, "clientY")) : (tipscope.x = chartOffset.left + this.series.chart.plotBox.x + this.plotX,
                    tipscope.y = chartOffset.top + this.series.chart.plotBox.y + this.plotY);
                break;
            case"scatter":
                tipscope.memberColor = this.series.color,
                    tipscope.metric1Header = this.series.xAxis.options.dimTitle.text,
                    tipscope.metric1Value = $$get(this, "series.xAxis.categories.length") > 0 ? this.series.xAxis.categories[this.x] : this.xMask ? this.xMask(this.x) : numberMask(this.x),
                    tipscope.metric2Header = this.series.yAxis.options.dimTitle.text,
                    tipscope.metric2Value = $$get(this, "series.yAxis.categories.length") > 0 ? this.series.yAxis.categories[this.y] : this.yMask ? this.yMask(this.y) : numberMask(this.y),
                    tipscope.labelHeader = this.series.chart.series[0].options.labelTitle.text,
                    tipscope.memberPoint = this.pointText, tipscope.hasPoint = defined(tipscope.labelHeader) && " " !== tipscope.labelHeader,
                    tipscope.colorHeader = this.series.chart.series[0].options.colorTitle.text,
                    tipscope.memberBreakBy = $$get(this, "series.chart.series.0.options.isPointName") ? null : this.series.name,
                    tipscope.hasColor = defined(tipscope.colorHeader) && this.series.chart.series[0].options.isDummyColor !== !0 && " " !== tipscope.colorHeader,
                    tipscope.sizeHeader = this.series.chart.series[0].options.sizeTitle.text,
                    tipscope.sizeValue = this.zMask ? this.zMask(this.z) : numberMask(this.z),
                    tipscope.hasSize = defined(tipscope.sizeHeader) && " " !== tipscope.sizeHeader,
                    tipscope.isColorLastField = tipscope.hasSize === !1,
                    tipscope.isPointLastField = tipscope.isColorLastField && tipscope.hasColor === !1,
                    tipscope.isYLastField = tipscope.isPointLastField && tipscope.hasPoint === !1,
                    tipscope.xPaddingBottom = "8px", tipscope.yPaddingBottom = tipscope.isYLastField ? "0px" : "8px",
                    tipscope.pointPaddingBottom = tipscope.isPointLastField ? "0px" : "8px",
                    tipscope.colorPaddingBottom = tipscope.isColorLastField ? "0px" : "8px",
                    tipscope.sizePaddingBottom = "0px", tipscope.x = $$get(ev, "currentTarget.tooltipPos.0") || $$get(document, "mousePosition.x") || $$get(event, "clientX"),
                    tipscope.y = $$get(ev, "currentTarget.tooltipPos.1") || $$get(document, "mousePosition.y") || $$get(event, "clientY")
        }
        tipper.activate({x: tipscope.x, y: tipscope.y, space: 0}), this.series.chart.sisenseTipper = {familyType: familyType, type: this.series.type, tipper: tipper, status: "on"}

        var member = this.category;

        var jaql = $.extend(w.metadata.panel('categories').items[0].jaql,{});
        jaql.filter = {};
        jaql.filter.members = [member];
        var filter = {
            jaql: jaql
        }
        var widget = _.find(w.dashboard.widgets.$$widgets, function(w){return w.subtype = 'pie/classic'})
        if(!widget) return;
        var filters = widget.metadata.panel('filters').items;
        while(filters.length){
            filters.pop();
        }
        filters.push(filter);
        widget.refresh();
    }
    var mouseOut = function(ev){
        var widget = w;
        var widget = _.find(w.dashboard.widgets.$$widgets, function(w){return w.subtype = 'pie/classic'})
        if(!widget) return;
        var filters = widget.metadata.panel('filters').items;
        while(filters.length){
            filters.pop();
        }
        console.log('mouseOut');
    }

    ev.result.plotOptions.series.point.events.mouseOver = hoverOverMarker;
    ev.result.plotOptions.series.point.events.mouseOut = mouseOut;
})