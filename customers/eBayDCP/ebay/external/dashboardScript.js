/**
 * Created by odeckelbaum on 10/30/14.
 */

window.ebayGlobal ={}
window.ebayGlobal.sortedChannelTypes = {};
window.ebayGlobal.sortedChannelTypes["paid display"] = 0;
window.ebayGlobal.sortedChannelTypes["unknown channel l1"] = 1;
window.ebayGlobal.sortedChannelTypes["tv"] = 2;
window.ebayGlobal.sortedChannelTypes["paid social"] = 3;
window.ebayGlobal.sortedChannelTypes["magazines"] = 4;
window.ebayGlobal.sortedChannelTypes["newspaper"] = 5;
window.ebayGlobal.sortedChannelTypes["radio"] = 6;
window.ebayGlobal.sortedChannelTypes["cinema"] = 7;
window.ebayGlobal.sortedChannelTypes["other"] = 8;
window.ebayGlobal.sortedChannelTypes["outdoor"] = 9;
window.ebayGlobal.sortedChannelTypes["press"] = 10;

//remove lines in the dashboard;
dashboard.on('refreshstart',function(d,e){

    $('.ui-resizable-handle',element).hide();
    $('widget-header',element).hide();
    $('.content',element).css('background-color','white')
})
