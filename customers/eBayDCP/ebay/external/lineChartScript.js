//column chart script!

widget.on('processresult', function(w,ev){

    if(window.hovered){ // responsible to keep tooltip alive
        window.hovered = null;
    }
    // generate tooltip template

    var mouseOver = function (ev) {
        window.hovered = w.oid;

        var member = this.category; // hovered member
        var jaql = $.extend({},w.metadata.panel('x-axis').items[0].jaql); // get jaql item of dimension
        jaql.filter = {};
        jaql.filter.members = [member];
        var filter = {
            jaql: jaql
        }
        // get the pie chart
        var pieWidgets = _.filter(w.dashboard.widgets.$$widgets, function(w){return  (w.subtype == 'pie/classic') || (w.subtype =='pie/donut') || (w.subtype =='pie/ring')})
        if(!pieWidgets || !pieWidgets.length) return;
        var widget;

        for(var i = pieWidgets.length - 1;i >= 0 ;i--){
            if(checkIfInTheSameRow(w,pieWidgets[i])){
                widget = pieWidgets[i];
            }
        }
        if(!widget){
            return;
        }
        var filters = widget.metadata.panel('filters').items;
        while(filters.length){
            filters.pop();
        }
        filters.push(filter);

        widget.refresh();



        var $dom = w.$dom;

        var familyType = 'cartesian';
        var template = generateCartesianTooltip();
        var tipper;
        var tipscope = {};
        tipper = $dom.tip({scope: tipscope, template: template}, {css: "charts", placement: {place: "b", anchor: "l"}, initialShowDelay: 5, betweenShowDelay: 10})

        tipscope.category = this.category;
        var plotBands = $$get(this, "series.xAxis.options.plotBands"), numOfPlotBands = $$get(plotBands, "length");
        if (numOfPlotBands > 0) {
            tipscope.hasTwoXAxis = !0;
            for (var i = 0; numOfPlotBands > i; i++)if (this.x > plotBands[i].from && this.x < plotBands[i].to) {
                tipscope.plotBand = $$get(plotBands[i], "label.text");
                break
            }
        } else tipscope.hasTwoXAxis = !1;
        tipscope.points = [];
        var i, point, mask, series = $$get(this, "series.chart.series"), stackTotal = this.stackTotal, point = {}, stackTotal = this.stackTotal, mask = $$get(this, "series.options.mask");
        point.seriesName = $$get(this, "series.options.onlyMeasuresInSeries") === !0 ? series[this.x].name : this.series.name, point.value = defined(mask) ? mask(this.y) : this.y,
            point.valueColor = defined(this.color) ? this.color : this.series.color,
            point.showPercentage = defined(stackTotal) && 0 !== stackTotal,
            point.showPercentage === !0 && (point.percentage = Math.round(this.y / stackTotal * 100).toFixed(0) + "%"), tipscope.points.push(point);
        var chartOffset = $(this.series.chart.renderTo).offset();
        "column" === this.series.type || "bar" === this.series.type ? (tipscope.x = $$get(ev, "currentTarget.tooltipPos.0") || $$get(document, "mousePosition.x") || $$get(event, "clientX"),
            tipscope.y = $$get(ev, "currentTarget.tooltipPos.1") || $$get(document, "mousePosition.y") || $$get(event, "clientY")) : (tipscope.x = chartOffset.left + this.series.chart.plotBox.x + this.plotX,
            tipscope.y = chartOffset.top + this.series.chart.plotBox.y + this.plotY);

        tipper.activate({x: tipscope.x, y: tipscope.y, space: 0});
        this.series.chart.sisenseTipper = {familyType: familyType, type: this.series.type, tipper: tipper, status: "on"};


    }

    var mouseOut = function(ev){

        window.hovered = null;

        var pieWidgets = _.filter(w.dashboard.widgets.$$widgets, function(w){return  (w.subtype == 'pie/classic') || (w.subtype =='pie/donut') || (w.subtype =='pie/ring')})
        if(!pieWidgets || !pieWidgets.length) return;
        var widget;

        for(var i = pieWidgets.length - 1;i >= 0 ;i--){
            if(checkIfInTheSameRow(w,pieWidgets[i])){
                widget = pieWidgets[i];
            }
        }
        if(!widget){
            return;
        }

        var filters = widget.metadata.panel('filters').items;
        while(filters.length){
            filters.pop();
        }
        widget.refresh();
    }

    ev.result.plotOptions.series.point.events.mouseOver = mouseOver;
    ev.result.plotOptions.series.point.events.mouseOut = mouseOut;

    ev.result.legend.x = 175;
    if(ev.rawResult.headers[0] == "DSC_MONTH"){
        sortMonths(ev);

    }
    else{
        sortWeeks(ev);
    }
    var series  = ev.result.series;
    var colors = ["#8b8bc2", "#ff8380", "#00cee6", "#fbb755", "#64c44d"];
    for(var i = 0 ; i< series.length; i++){
        var j = i;
        j = j == colors.length ? 0 : j;
        series[i].color = colors[j]
    }
})

var sortMonths = function(ev){
    var months =
    {
        "january" : 0,
        "february" : 1,
        "march" : 2,
        "april" : 3,
        "may" : 4,
        "june" : 5,
        "july" : 6,
        "august" : 7,
        "september" : 8,
        "october" : 9,
        "november" : 10,
        "december" : 11
    };

    var categories = ev.result.xAxis.categories;
    var series = ev.result.series
    var sortedCategories = categories.slice(0);
    sortedCategories.sort(function(a,b){ return months[a.toLowerCase()] - months[b.toLowerCase()] });


    for(var i = 0;i<series.length; i++){
        var sortableArray = [];
        var data = series[i].data;
        for(var j = 0;j < data.length; j++){

            var obj = {
                data: data[j],
                index:months[categories[j].toLowerCase()]
            }

            sortableArray.push(obj);

        }

        sortableArray.sort(function(a,b){return a.index - b.index});
        series[i].data =  sortableArray.map(function(d){ return d.data});
    }

    ev.result.xAxis.categories = sortedCategories;
}
var sortWeeks = function(ev){
    var weeks = {};
    var numberOfWeeks = 53;
    for(var i = 0; i < numberOfWeeks; i++){
        weeks['w'+(i+1)] = i;
    }

    var categories = ev.result.xAxis.categories;
    var series = ev.result.series
    var sortedCategories = categories.slice(0);
    sortedCategories.sort(function(a,b){ return weeks[a.toLowerCase()] - weeks[b.toLowerCase()] });


    for(var i = 0;i < series.length; i++){
        var sortableArray = [];
        var data = series[i].data;
        for(var j = 0;j < data.length; j++){
            var obj = {
                cat: categories[j],
                data: data[j],
                index:weeks[categories[j].toLowerCase()]
            }

            sortableArray.push(obj);

        }

        sortableArray.sort(function(a,b){return a.index - b.index});
        series[i].data =  sortableArray.map(function(d){ return d.data});
    }

    ev.result.xAxis.categories = sortedCategories;
}

var checkIfInTheSameRow = function(widget1, widget2){
    var dashboard = widget1.dashboard;
    var columns = dashboard.layout.columns;
    for(var i=0; i< columns.length;i++){
        for(var j=0;j<columns[i].cells.length;j++){
            var subcells = columns[i].cells[j].subcells;
            if(checkWidgetsInSubCells(subcells,widget1,widget2)){
                return true;
            }
        }
    }
    return false;
}

var checkWidgetsInSubCells = function(subcells, widget1, widget2){
    var widget1Found;
    var widget2Found;
    for(var i = 0;i< subcells.length;i++){
        if(subcells[i].elements[0].widgetid == widget1.oid) {
            widget1Found = true;
            if(widget2Found) return true;
        }
        if(subcells[i].elements[0].widgetid == widget2.oid) {
            widget2Found = true;
            if(widget1Found) return true;
        }
    }
    return false;
}

var generateCartesianTooltip = function () {
    return '<div class="cartesian-tooltip">' +
        '<div class="cartesian-tooltip-values" ng-repeat="item in model.points">' +
        '<div class="cartesian-tooltip-seriesName"> {{item.seriesName}} </div> ' +
        '<div class="cartesian-tooltip-singleLineValue" style="color:{{item.valueColor}}"> ' +
        '<div class="cartesian-tooltip-value"> {{item.value}} </div> <div class="cartesian-tooltip-percentage" data-ng-show="item.showPercentage"> / {{item.percentage}} </div> </div>' +
        '</div>' + // values
        '<div class="highcharts-tooltip-line-separator"></div>' + // line separator
        '<div class="cartesian-tooltip-xAxis"> <div class="cartesian-tooltip-plotBand" data-ng-show="model.hasTwoXAxis"> {{model.plotBand}},&nbsp </div> ' +
        '<div class="cartesian-tooltip-category">{{model.category}} </div> </div>' + // first line - xAxis details
        '</div>';

};