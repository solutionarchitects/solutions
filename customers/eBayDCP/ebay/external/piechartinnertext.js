/*
 Welcome to your Widget's Script.

 - You can access your Widget via the 'widget' variable name.
 - You can access your Widget's DOM via the 'element' variable name (undefined until DOM creation).
 - You can access your Dashboard by accessing the 'dashboard' variable name.

 - For a complete API reference for Widgets and Dashboards go here: https://docs.google.com/document/d/1nQBZtWAdNFAd9nBhPWGVT3qOMS4Qm0PzBZVIzz5DfE8/
 */
function getFormattedDate(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
}
widget.on('processresult', function(w, e){

    e.result.plotOptions.pie.innerSize = '70%';

    var onLoad = function(ev,chart){

        $(this.container).parent().append('<div id="addText"></div>');
        $('#addText').css({
            'position':'absolute',
            'left':'0px',
            'top':'0px',
            'vertical-align':'middle',
            //'font-family':'Ubuntu', 'sans-serif', 'serif'
            'text-align':'center'
        })

        var textX = this.plotLeft + (this.series[0].center[0]);
        var textY = this.plotTop  + (this.series[0].center[1]);


        var upperText = $$get(w.metadata.panel('filters'),'items.0.jaql.filter.members.0');
        upperText  = upperText && upperText.getMonth() ? getFormattedDate(upperText) :'';
        var sum = 0;
        for(var i = w.rawQueryResult.values.length - 1; i>=0;i--){
            sum += w.rawQueryResult.values[i][1].data;
        }

        var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
        span += '<span style="font-size: 13px">'+ upperText +'</span><br>';
        span += '<hr>';
        span += '<span style="font-size: 13px">'+ abbrNum(sum,2) +'</span>';
        span += '</span>';

        $("#addText").append(span);
        span = $('#pieChartInfoText');
        span.css('left', textX + (span.width() * -0.5));
        span.css('top', textY + 25 + (span.height() * -0.5));

        $(".highcharts-legend-item path", this.container)
            .attr("stroke-width", "12")
            .attr("width", "12")
            .attr("d", "M 0 12 L 12 12"),
            $(".highcharts-legend-item rect", this.container)
                .attr("rx", "0")
                .attr("ry", "0")
                .attr("width", "12")
                .attr("height", "12")
    }
    e.result.chart.events.load =onLoad;

    /*
     e.result.title =  {
     verticalAlign: 'middle',
     floating: true,
     align:'center',
     text:'<span style="color=red;">Upper</span> <br><hr> bottom',
     //style:{ "color": "#333333", "fontSize": "25px" },
     useHTML:true,
     x:18,
     y:-10
     }*/
})



function abbrNum(number, decPlaces) {
    // 2 decimal places => 100, 3 => 1000, etc
    decPlaces = Math.pow(10,decPlaces);

    // Enumerate number abbreviations
    var abbrev = [ "k", "m", "b", "t" ];

    // Go through the array backwards, so we do the largest first
    for (var i=abbrev.length-1; i>=0; i--) {

        // Convert array index to "1000", "1000000", etc
        var size = Math.pow(10,(i+1)*3);

        // If the number is bigger or equal do the abbreviation
        if(size <= number) {
            // Here, we multiply by decPlaces, round, and then divide by decPlaces.
            // This gives us nice rounding to a particular decimal place.
            number = Math.round(number*decPlaces/size)/decPlaces;

            // Handle special case where we round up to the next abbreviation
            if((number == 1000) && (i < abbrev.length - 1)) {
                number = 1;
                i++;
            }

            // Add the letter for the abbreviation
            number += abbrev[i];

            // We are done... stop
            break;
        }
    }

    return number;
}

