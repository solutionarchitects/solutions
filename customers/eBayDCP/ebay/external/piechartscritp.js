prism.removeOnContainerClick(widget);


widget.on('processresult', function(w, e){
    e.result.chart.spacing = [-5,0,50,0]
    e.result.plotOptions.pie.innerSize = '70%';

    var onLoad = function(ev,chart){

        $(this.container).parent().append('<div id="addText"></div>');
        $('#addText').css({
            'position':'absolute',
            'left':'0px',
            'top':'0px',
            'vertical-align':'middle',
            //'font-family':'Ubuntu, sans-serif 'serif'
            'text-align':'center'
        })

        var textX = this.plotLeft + (this.series[0].center[0]);
        var textY = this.plotTop  + (this.series[0].center[1]);


        var upperText = $$get(w.metadata.panel('filters'),'items.0.jaql.filter.members.0');
        upperText  = upperText && upperText.getMonth() ? prism.ebayGlobals.getFormattedDate(upperText) :'';
        var sum = 0;
        for(var i = w.rawQueryResult.values.length - 1; i>=0;i--){
            sum += w.rawQueryResult.values[i][1].data;
        }

        var span = '<span id="pieChartInfoText" style="position:absolute; text-align:center;">';
        span += '<span style="font-size: 13px">'+ upperText +'</span><br>';
        span += '<hr>';
        span += '<span style="font-size: 13px">'+ prism.ebayGlobals.abbrNum(sum,2) +'</span>';
        span += '</span>';

        $("#addText").append(span);
        span = $('#pieChartInfoText');
        span.css('left', textX + (span.width() * -0.5));
        span.css('top', textY + 25 + (span.height() * -0.5));

        $(".highcharts-legend-item path", this.container)
            .attr("stroke-width", "12")
            .attr("width", "12")
            .attr("d", "M 0 12 L 12 12"),
            $(".highcharts-legend-item rect", this.container)
                .attr("rx", "0")
                .attr("ry", "0")
                .attr("width", "12")
                .attr("height", "12")
    }
    e.result.chart.events.load =onLoad;
})
