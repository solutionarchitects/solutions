function () {
    var chart = this,
        axes = chart.axes,
        renderer = chart.renderer,
        options = chart.options;

    var labels = options.labels,
        credits = options.credits,
        creditsHref;

    // remove old tooltip (in case of selection made)
    //$('.tipper-host').remove();

    // Title
    chart.setTitle();

    // Legend
    chart.legend = new Legend(chart, options.legend);

    chart.getStacks(); // render stacks

    // Get margins by pre-rendering axes
    // set axes scales
    each(axes, function (axis) {
        axis.setScale();
    });

    chart.getMargins();

    chart.maxTicks = null; // reset for second pass
    each(axes, function (axis) {
        axis.setTickPositions(true); // update to reflect the new margins
        axis.setMaxTicks();
    });
    chart.adjustTickAmounts();
    chart.getMargins(); // second pass to check for new labels


    // Draw the borders and backgrounds
    chart.drawChartBox();


    // Axes
    if (chart.hasCartesianSeries) {
        each(axes, function (axis) {
            axis.render();
        });
    }

    // The series
    if (!chart.seriesGroup) {
        chart.seriesGroup = renderer.g('series-group')
            .attr({ zIndex: 3 })
            .add();
    }
    each(chart.series, function (serie) {
        serie.translate();
        serie.setTooltipPoints();
        serie.render();
    });

    // Labels
    if (labels.items) {
        each(labels.items, function (label) {
            var style = extend(labels.style, label.style),
                x = pInt(style.left) + chart.plotLeft,
                y = pInt(style.top) + chart.plotTop + 12;

            // delete to prevent rewriting in IE
            delete style.left;
            delete style.top;

            renderer.text(
                    label.html,
                    x,
                    y
                )
                .attr({ zIndex: 2 })
                .css(style)
                .add();

        });
    }

    // Credits
    if (credits.enabled && !chart.credits) {
        creditsHref = credits.href;
        chart.credits = renderer.text(
                credits.text,
                0,
                0
            )
            .on('click', function () {
                if (creditsHref) {
                    location.href = creditsHref;
                }
            })
            .attr({
                align: credits.position.align,
                zIndex: 8
            })
            .css(credits.style)
            .add()
            .align(credits.position);
    }

    // Set flag
    chart.hasRendered = true;

}