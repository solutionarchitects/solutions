widget.on('beforeviewloaded', function(sender, args) {
    if (!defined(args.widget.rawQueryResult)){
        return;
    }

    args.options.xAxis.title = {text:args.widget.rawQueryResult.headers[0], style:{color : "#919191"}};
    args.options.yAxis[0].title = {text : args.widget.queryResult.series[0].name, style:{color : "#919191"}};
});