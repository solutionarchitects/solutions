widget.on("render", function(widget, args){
    if (args.widget.queryResult.series.length > 0){
        args.widget.queryResult.yAxis[1].title = {text : args.widget.queryResult.series[0].name, style:{color : "#919191"}};
    }

    if (args.widget.queryResult.series.length > 1){
        args.widget.queryResult.yAxis[0].title = {text : args.widget.queryResult.series[1].name, style:{color : "#919191"}};
    }
});