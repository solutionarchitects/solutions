widget.on('beforeviewloaded', function(sender, args) {
    if (!defined(args.options.xAxis)){
        return;
    }

    args.options.xAxis.title = {text:args.options.xAxis.dimTitle.text, style:{color : "#919191"}};
    args.options.yAxis[0].title = {text : args.options.yAxis[0].dimTitle.text, style:{color : "#919191"}};
    args.options.legend = {enabled:false};
});

widget.on('processresult', function(sender, args) {
    args.result.series[0].regression = true;
    args.result.series[0].regressionSettings = {
        type: 'linear',
        color:  '#29a2a4',
        dashStyle : 'dash',
        name: ' '
    };
});