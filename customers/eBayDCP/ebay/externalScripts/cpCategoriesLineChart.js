prism.plotBandChart(widget, {seriesIndex:2, threshold:0, plotBandLegend:[{color:"#9CE38D", name:"Sig. Pos. Correlation"}, {color:"#FB8E8A", name:"Sig. Neg. Correlation"}]});

prism.plotBandChart(widget, {seriesIndex:3, threshold:0.1, plotBandLegend:[{color:"#FAFAFA", name:"Non Significant"}]});


widget.on("render", function(widget, args){
    if (args.widget.queryResult.series.length > 0){
        args.widget.queryResult.yAxis[0].title = {text : args.widget.queryResult.series[0].name, style:{color : "#919191"}};
    }

    if (args.widget.queryResult.series.length > 1){
        args.widget.queryResult.yAxis[1].title = {text : args.widget.queryResult.series[1].name, style:{color : "#919191"}};
    }
});

widget.on("processresult",function(widget, args){
    for (i=0; i < args.result.xAxis.categories.length; ++i){
        args.result.xAxis.categories[i] = "W" + args.result.xAxis.categories[i];
    }
});