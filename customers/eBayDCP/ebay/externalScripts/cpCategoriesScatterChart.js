prism.scatterChartFillPanel(widget, {threshold : 0.1, bellowThresholdText : "Sig.", aboveThresholdText : "Non Sig." });

prism.registerFilterOnHover(widget, {panel : {name : "Break By / Color", index:0}, subtypes : ["line/basic"], data : {element : "selectionData", index : 3, titleElement : "selectionData", titleIndex : 3}});

widget.on('beforeviewloaded', function(sender, args) {
    if (!defined(args.options.xAxis)){
        return;
    }

    args.options.xAxis.plotLines = [{
        color: '#e6e6e6',
        width: 5,
        value: 0}];
    args.options.xAxis.min = -1;
    args.options.xAxis.max = 1;
    args.options.xAxis.tickInterval = 0.2;
    args.options.xAxis.title = {text:args.options.xAxis.dimTitle.text, style:{color : "#919191"}};
    args.options.yAxis[0].plotLines = [{
        color: '#e6e6e6',
        width: 5,
        value: 0.3}];
    args.options.yAxis[0].min = 0;
    args.options.yAxis[0].max = 1;
    args.options.yAxis[0].tickInterval = 0.1;
    args.options.yAxis[0].title = {text : args.options.yAxis[0].dimTitle.text, style:{color : "#919191"}};
    args.options.labels = {
        items : [{
            html : 'FLEXIBLE PRICE',
            style : {
                left : '200px',
                top : '220px',
                fontSize : '20px',
                color: '#e6e6e6'
            }},
            {
                html : 'COMPETITIVE PRICE',
                style : {
                    left : '800px',
                    top : '220px',
                    fontSize : '20px',
                    color: '#e6e6e6'
                }},
            {
                html : 'JUSTIFIED PRICE',
                style : {
                    left : '200px',
                    top : '350px',
                    fontSize : '20px',
                    color: '#e6e6e6'
                }},
            {
                html : 'PRICE TOO HIGH',
                style : {
                    left : '800px',
                    top : '350px',
                    fontSize : '20px',
                    color: '#e6e6e6'
                }
            }]
    };
});
