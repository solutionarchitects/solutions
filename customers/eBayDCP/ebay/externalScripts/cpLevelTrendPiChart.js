widget.on("beforeviewloaded", function(e, args){
    if (defined(args.widget.metadata.title)){
        args.options.title.text = args.widget.metadata.title;
    }
    else{
        args.options.title.text = "Total for period";
    }

    args.options.title.verticalAlign = "middle";
    args.options.title.floating = true;
});