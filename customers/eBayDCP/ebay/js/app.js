var app = angular.module('app', ['angularLoad', 'ngTouch', 'ui.bootstrap']);

app.controller('mainController', function($scope, $http, $q, $rootScope, dateTimeService) {   


    var loadDashDef;
    $scope.loaded;
    $scope.dashboardURL = null;
	$scope.dashboards = [];
	$scope.selectedDashboard = null;
	$scope._prism = null;

	function loadDashboards() {
        $http({
            method: 'get',
            url: 'data/dashboards.json',
            headers: {
                'Content-type': 'application/json'
            }
        }).success(function(data) {
        	angular.forEach(data.dashboards, function(dash) {
	            dash.frameUrl = dash.url + embedString();
	            if($scope.selectedDashboard  && $scope.selectedDashboard.caption.toLowerCase() != dash.caption.toLowerCase()){
	                dash.selected = ''
	            }
	            else if(dash.selected == 'selected'){
	                $scope.changeDashboard(dash);
	            }
        	});

            $scope.dashboards = data.dashboards;
        }).error(function (data, status, headers, config) {
            console.log('error loading filters');
        });
    };   

    var embedOptions = {
        embed: true,
        header: false,
        rightPane: false,
        leftPane: false,
        toolbar: false,
        volatile: true
    };

    function embedString(header, rightpane, leftpane, toolbar, volatile) {
        var embedString = '';
        embedString += '?embed='+  embedOptions.embed;
        embedString += rightpane || embedOptions.rightPane ? "" : "&r="+  embedOptions.rightPane;
        embedString += leftpane || embedOptions.leftPane ? "" : "&l="+  embedOptions.leftPane;
        embedString += header || embedOptions.header ? "" : "&h="+  embedOptions.header;
        embedString += toolbar || embedOptions.toolbar ? "" : "&t="+  embedOptions.toolbar;
        embedString += volatile || embedOptions.volatile ? "" : "&volatile=" +  embedOptions.volatile;
        return embedString
    };

    loadDashboards();

    $scope.changeDashboard = function(dash) { 
        angular.forEach($scope.dashboards, function(dashboard) {
            dashboard.selected = null;
        });  
        dash.selected = "selected";
    	$scope.selectedDashboard = dash;
        $scope.dashboardURL = $scope.selectedDashboard.frameUrl;
        
        loadDashDef = $q.defer();
        $scope.loaded = loadDashDef.promise;

    	$http({
            method: 'get',
            url: dash.filterList,
            headers: {
                'Content-type': 'application/json'
            }
        }).success(function(data) {
        	dash.filterGroups = data.filterGroups;
            compileDateFilters(dash.filterGroups); 
            
            $rootScope.$emit('dashboardLoaded');
        }).error(function (data, status, headers, config) {
            console.log('error loading filters');
            loadDeferred.reject();
        });
    }

	$scope.getPrism = function() {
		if(!$scope._prism) {
         	$scope._prism = $$get(window, 'frames.dashboard.prism');
    	}

    	return $scope._prism;
    };

    window.loadingFinished = function() {
        loadDashDef.resolve();
    }

    function compileDateFilters(filterGroups) {
        angular.forEach(filterGroups, function(group) {
            angular.forEach(_.where(group.jaqls, { type: "date" }), function(j) {
                j.filter.from = dateTimeService.getDateBySubtype(j.subtype);
                j.filter.to = moment(new Date()).format('YYYY-MM-DD');
            });
        });
    }
});

app.controller('filtersController', function($scope, $rootScope) {

	$scope.applyFilter = function(jaql){

        var currentFilters = $scope.getPrism().$ngscope.dashboard.filters.$$items;

        filter = checkCurrentFilters(currentFilters, jaql);

        if (!filter || filter.length == 0) {
            return;
        }

        try {
            $scope.getPrism().$ngscope.dashboard.filters.update(filter, {save: true, refresh: true})
        }
        catch (e) {
            console.log('Failed to filter dashboard')
        }
	}

    $scope.changeDateResolution = function(dim, level, datatype) {
        var widgets = $scope.getPrism().$ngscope.dashboard.widgets.$$widgets;
        if (widgets.length > 0) {
            angular.forEach(widgets, function(widget) {

                var currentPanel = (widget.metadata.panel('categories') && widget.type == 'chart/column') ? widget.metadata.panel('categories') : widget.metadata.panel('x-axis');

                if (!currentPanel) return;

                var currentJaql = currentPanel.items[0].jaql;
                if (!currentJaql.dim || !currentJaql.level || !currentJaql.datatype ){
                    return;
                }
                else if (currentJaql.dim == dim && currentJaql.datatype != "datetime") {
                    return;
                }
                else if(currentJaql.dim.toLowerCase() === dim.toLowerCase() && currentJaql.datatype.toLowerCase()  === datatype.toLowerCase() &&
                    currentJaql.level && level && currentJaql.level.toLowerCase() == level.toLowerCase()
                   ){
                    return;
                }
                else{
                    currentJaql.level = level;
                }
                
                widget.changesMade();
                widget.refresh();
            });
        }
    }

    $scope.resetClick = function() {
        var groups = [];
        angular.forEach($scope.selectedDashboard.filterGroups, function(fg) {
            groups.push({ caption : fg.caption, jaql : null });
        });

        resetFilters(groups);
    }

	function resetFilters(groups) {
        $scope.$broadcast('reset', groups);
	}

    function initializeExistingFilters() {
        var existingFilters = $scope.getPrism().$ngscope.dashboard.filters.$$items;
        var existingJaqls = _.map(existingFilters, function(f) {
            return f.jaql;
        });

        var groups = [];
        angular.forEach(existingJaqls, function(ej) {
            angular.forEach($scope.selectedDashboard.filterGroups, function(fg) {            
                if (ej.dim == fg.column) {
                    var j = null;

                    angular.forEach(fg.jaqls, function(jaql) {
                        if (ej.filter.all && jaql.filter.all) {
                            j = jaql;
                            return;
                        }
                        else if (ej.filter.members && jaql.filter.members) {
                            angular.forEach(ej.filter.members, function(m) {
                                jaql.filter.members.push(m);
                            });
                            
                            j = jaql;
                            return;
                        }
                        else if ((ej.filter.top && jaql.filter.top) && (ej.filter.top == jaql.filter.top) && (ej.filter.by.dim == jaql.filter.by.dim)) {
                            j = jaql;
                            return;
                        }
                        else if ((ej.filter.to && ej.filter.from) && jaql.type == "dateRange") {
                            jaql.filter.from = ej.filter.from;
                            jaql.filter.to = ej.filter.to;

                            j = jaql;
                            return;
                        }
                    })

                    groups.push({ caption : fg.caption, jaql : j });
                    return;
                }
            });
        });

        resetFilters(groups);
    }

    /*
    $scope.setCascadingFilter = function(jaql, affectedGroup) {
        var group = _.findWhere($scope.selectedDashboard.filterGroups, { caption : affectedGroup });
        group.isDirty = true;

        var cascadingFilter = { 
                dim : jaql.dim,
                datatype: jaql.datatype, 
                merged : true,
                filter : jaql.filter
            }

        var previousCF = _.findWhere(group.cascadingFilters, { dim : jaql.dim });
        if(previousCF != undefined) {
            var i = group.cascadingFilters.indexOf(previousCF);
            group.cascadingFilters.splice(i, 1);
        }                   

        if(!jaql.filter.all) {
            group.cascadingFilters.push(cascadingFilter)
        }

        $scope.$broadcast('reset', [affectedGroup]);
    }
    */

    $scope.getCascadingFilter = function(affectingGroup) {
        var group = _.findWhere($scope.selectedDashboard.filterGroups, { caption : affectingGroup });
        var affectingJaql = _.findWhere(group.jaqls, { selected : true });

        var cascadingFilter = { 
                dim : affectingJaql.dim,
                datatype: affectingJaql.datatype, 
                merged : true,
                filter : affectingJaql.filter
            }

        return cascadingFilter;
    }

    function checkCurrentFilters(currentFilters, filters) {

        if (Array.isArray(filters)) {
            var filtersCopy = filters.slice(0);
            for (var i = filters.length - 1; i >= 0; i--) {
                for (var j = currentFilters.length - 1; j >= 0 && i >= 0; j--) {
                    if (currentFilters[j].jaql.dim.toLowerCase() == filters[i].jaql.dim.toLowerCase()) {
                        if (isFilterRelevant(currentFilters[j], filters[i])) {
                            currentFilters.splice(j, 1);
                        }
                        else {
                            filtersCopy.splice(i, 1);
                        }

                    }
                }
            }
            return filtersCopy;
        }
        //filters is not an array - single filter
        else {
            for (var j = currentFilters.length - 1; j >= 0; j--) {
                if (currentFilters[j].jaql.dim.toLowerCase() == filters.jaql.dim.toLowerCase()) {
                    if (isFilterRelevant(currentFilters[j], filters)) {
                        currentFilters.splice(j, 1);
                    }
                    else {
                        return null
                    }

                }
            }
        }
        return filters;
    }

    function isFilterRelevant(currentFilter, filter) {
        if (currentFilter.jaql.filter.all && filter.jaql.filter.all) {
            return false
        }
        else if (currentFilter.jaql.filter.all || filter.jaql.filter.all) {
            return true
        }
        else if (currentFilter.jaql.filter.from && currentFilter.jaql.filter.to && filter.jaql.filter.from && filter.jaql.filter.to) {
            return !(currentFilter.jaql.filter.from == filter.jaql.filter.from && currentFilter.jaql.filter.to == filter.jaql.filter.to &&
            filter.level == currentFilter.level);
        }
        else if ((filter.jaql.filter.from && filter.jaql.filter.to) || (currentFilter.jaql.filter.from && currentFilter.jaql.filter.to)) {
            return true;
        }
        else if (currentFilter.jaql.dim.indexOf('Calendar') > -1 && filter.jaql.dim.indexOf('Calendar') > -1) {
            if (currentFilter.jaql.filter.members[0].getMonth) {
                // im creating new date here so the date object can inherit the custom function i created
                return !(new Date(currentFilter.jaql.filter.members[0]).ToFilterFormat() == filter.jaql.filter.members[0] &&
                currentFilter.jaql.level.toLowerCase() == filter.jaql.level.toLowerCase());
            }
        }
        return !filter.jaql.filter.members.equals(currentFilter.jaql.filter.members);
    };

    // Initiate
    $rootScope.$on('dashboardLoaded', function () {
        $scope.loaded.then(function() {
    	   initializeExistingFilters();
        });
	});
});

app.controller('jaqlMemberController', function($scope) {
	$scope.isOpen = false;
    $scope.toggled = function(open) {
        if(open) {
            $scope.$broadcast('open');
        }
    }

    $scope.$on('reset', function (e, args) {
        $scope.isOpen = false;
    });
});