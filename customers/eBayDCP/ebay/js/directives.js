app.directive('filterGroup', function() {
	return {
		restrict: 'E',
		templateUrl : '/ebay/templates/filterGroupTemplate.html',
		controller : function($scope, $http, $element) {
			$scope.onHeaderClick = function() {
				$scope.isCollapsed = !$scope.isCollapsed;
			};

			$scope.selectMember = function(jaql) {
				var popUpMembers = ['dateRange', 'memberSelection'];
				if (popUpMembers.indexOf(jaql.type) == - 1) {
					$scope.setSelectedMember(jaql);
				}
			};

			$scope.setSelectedMember = function(jaql) {
				reset(jaql);

				angular.forEach($scope.filter.affect, function(ef) {
			        var group = _.findWhere($scope.selectedDashboard.filterGroups, { caption : ef });
        			group.isDirty = true;
				});
			}

			function reset(jaql) {				
				angular.forEach($scope.filter.jaqls, function(j) {
					j.selected = false;
				});

				if(jaql) {
					$scope.selectedJaql = jaql;
				}
				else {
					$scope.selectedJaql = _.findWhere($scope.filter.jaqls, { caption: $scope.filter.defaultJaql});
				}

				var filterJaql = { 
					jaql : {
						dim : $scope.selectedJaql.dim, 
						datatype : $scope.selectedJaql.datatype, 
						filter : $scope.selectedJaql.filter,
						level : $scope.selectedJaql.level
					} 
				}

				$scope.applyFilter(filterJaql);
				
				if ($scope.selectedJaql.level) {
					$scope.changeDateResolution($scope.selectedJaql.dim, $scope.selectedJaql.resolutionLevel, $scope.selectedJaql.datatype);
				}

				$scope.selectedJaql.selected = true;
				$scope.isCollapsed = true;
			}

			$scope.$on('reset', function (e, args) {
				var filter = _.findWhere(args, { caption : $scope.filter.caption });
				if (filter !== undefined) {
					reset(filter.jaql);
				}
				else if (args.length == 0) {
					reset();
				}
			})
		}	
	};
});

app.directive('ebayMemberSelector', function() {
	return {
		restrict: 'E',
		templateUrl : '/ebay/templates/memberSelectorTemplate.html',
		controller : function ($scope, $http, $q) {
			$scope.members = [];
			$scope.searchText = null;

			$scope.selectMemberFromList = function(member) {
				member.selected = !member.selected;

				if(member.selected) {
					if ($scope.jaqlItem.filter.members.indexOf(member.text) == -1) {
						$scope.jaqlItem.filter.members.push(member.text);
					}
				}
				else { 
					var index = $scope.jaqlItem.filter.members.indexOf(member.text);
					$scope.jaqlItem.filter.members.splice(index, 1);
				}

				if ($scope.jaqlItem.filter.all !== undefined) {
					delete $scope.jaqlItem.filter.all;
				}				
			}

			$scope.clickOK = function(){
				if ($scope.getSelectionCount() == $scope.members.length) {
					$scope.jaqlItem.filter.members = [];
					$scope.jaqlItem.filter.all = true;
				}

				$scope.setSelectedMember($scope.jaqlItem)
				$scope.isOpen = false;
			}

			$scope.getSelectionCount = function() {
				return $scope.getSelectedMembers().length;
			}

			$scope.getSelectedMembers = function() {
				return _.where($scope.members, {selected:true});
			}

			$scope.setSelectionCount = function() {
				if ($scope.getSelectionCount() < $scope.members.length) {
					angular.forEach($scope.members, function(m) {
						m.selected = true;
					});
				}
				else {
					angular.forEach($scope.members, function(m) {
						m.selected = false;
					});

					$scope.jaqlItem.filter.members = [];	
				}
			}

			$scope.searchMembers = function() {
				var containsFilter = { contains : $scope.searchText }
				getDimentionMembers(containsFilter);
			}

			function getDimentionMembers(filter) {
				var defer = $q.defer();
				var jaql = {};

		        jaql.datasource = $scope.selectedDashboard.datasource;
		        jaql.metadata = [{
		        	"dim" : $scope.jaqlItem.dim
		        }];

		        if(filter) {		        	
		        	jaql.metadata[0].filter = filter;
		        }

	        	angular.forEach($scope.filter.affectedBy, function(ab) {
	        		var cf = $scope.getCascadingFilter(ab);
	        		jaql.metadata.push(cf);
	        	});

		        $http.post('/jaql/query', jaql)
		        	.success(function (res) {
		        		$scope.members = _.map(res.values, function(val) {
	        				var selected = $scope.jaqlItem.filter.members.indexOf(val[0].text) != -1;
		        			return { text : val[0].text, selected : selected };
		        		});
		        		$scope.filter.isDirty = false;
		        		defer.resolve();
		        	})
		        	.error(function (error) {
	        			console.log(error);
	        			defer.reject();
		        	})

		        return defer.promise;
			}			

			$scope.$on('open', function(){
				if($scope.filter.isDirty === undefined || $scope.filter.isDirty) {
					getDimentionMembers();					
				}
			})
		}	
	};
});

app.directive('dateRangeSelector', function() {
	return {
		restrict: 'E',
		templateUrl : '/ebay/templates/dateRangeSelectorTemplate.html',
		controller : function ($scope) {
			$scope.fromDate = null;
			$scope.toDate = null;

			$scope.clickOK = function() {
				if ($scope.fromDate  && $scope.toDate) {
					$scope.jaqlItem.filter.from = $scope.fromDate;
					$scope.jaqlItem.filter.to = $scope.toDate;

					$scope.setSelectedMember($scope.jaqlItem)
					$scope.isOpen = false;
				}
			}

			$scope.$on('open', function(){
				if (!$scope.fromDate && !$scope.toDate) {
					$scope.fromDate = $scope.jaqlItem.filter.from != "" ? moment($scope.jaqlItem.filter.from).toDate() : null;
					$scope.toDate = $scope.jaqlItem.filter.to != "" ? moment($scope.jaqlItem.filter.to).toDate() : null;
				}
			})		
		}	
	};
});

app.directive('iframeLoad', function(){
	return {
		restrict : 'A',
		link : function(scope, element, attrs) {
				element[0].onload = function() {
					scope.$eval(attrs.iframeLoad)
				};
			
		}
	}
})