app.service('dateTimeService', function() {
	var yearToDate = function() {
		return new Date(new Date().getFullYear(), 0, 1, 0, 0, 0, 0);
	}

	var quarterToDate = function() {
		var now = new Date();
		var month = now.getMonth();
		var quarter = parseInt(month / 3) + 1;

		var quarterStartMonth = 0;
			        
        switch (quarter) {
            case 1:
            	quarterStartMonth = 0;
            	break;
        	case 2:
        		quarterStartMonth = 3;
        		break;
    		case 3:
    			quarterStartMonth = 6;
    			break;
    		case 4:
    			quarterStartMonth = 9;
    			break;
		}

		return new Date(now.getFullYear(), quarterStartMonth, 1, 0, 0, 0, 0);
	}

	var monthToDate = function() {
		var now = new Date();
		return new Date(now.getFullYear(), now.getMonth(), 1, 0, 0, 0, 0);
	}

	var past12Months = function() {
		var now = new Date(),
			year = now.getFullYear();

		return now.setFullYear(--year);
	}

	var past12Weeks = function() {
		var returnDate = new moment(Date());
		returnDate.weekday(0);
		returnDate.week(returnDate.week() - 11);

		return returnDate;
	}

	var past30Days = function() {
		var now = new Date(),
			date = now.getDate(),
			diff = date - 30;

		if (diff < 0) {
			now.setDate(0);
			diff = Math.abs(diff);
			date = now.getDate() - diff;
			now.setDate(date);
		}
		else {
			now.setDate(diff);
		}

		return now;
	}

	this.getDateBySubtype = function(subtype) {
		var date = null;

		switch (subtype) {
			case "YTD" :
				date = yearToDate();
				break;
			case "QTD" :
				date = quarterToDate();
				break;
			case "MTD" :
				date = monthToDate();
				break;
			case "P12M" :
				date = past12Months();
				break;
			case "P12W" :
				date = past12Weeks();
				break;
			case "P30D" :
				date = past30Days();
				break;
		}

		return moment(date).format('YYYY-MM-DD');
	}
})