/**
 * Created by Admin on 22-Oct-14.
 */
var app = angular.module('app', ['angularLoad', 'dashboardTabber','filters','ngTouch']);

app.service('sharedApp', function($rootScope) {

        $.blockUI.defaults.overlayCSS.backgroundColor = '#fafafa';
        $.blockUI.defaults.overlayCSS.opacity = 1;
        $.blockUI.defaults.css = {

            padding: 0,
            margin: 0,
            width: '30%',
            top: '40%',
            left: '35%',
            textAlign: 'center',
            cursor: 'wait'
        };

    $.blockUI({
        message: '<img src="/ebay/resources/images/widget-loader.gif" style="width: 48px; height: 48px;" />'
    });

    var sharedService = {};

    sharedService.updateState = function(state){
        $rootScope.$broadcast('stateUpdated', state);
    };
    //sharedService.currentDashboard = {};

    sharedService.updateCurrentDashboard = function(dashboard) {
        if(DEBUG_MODE){
            console.log('Updating new dashboard ' + dashboard.caption)
        }
        if(!$.isEmptyObject(this.currentDashboard)){
            this.currentDashboard.selected = '';
        }

        dashboard.selected = 'selected';
        this.currentDashboard = dashboard;
        this.broadcastDashboardUpdated(dashboard);
    };

    function toTitleFormat(str)
    {
        return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
    }

    sharedService.broadcastDashboardUpdated = function(dashboard) {
        if(DEBUG_MODE){
            console.log('Dashboard updated ' + dashboard.caption)
        }
        document.title = toTitleFormat(dashboard.caption);
        $rootScope.$broadcast('dashboardUpdated', dashboard);
    };

    if(DEBUG_MODE){
        console.log('Initialized sharedApp')
    }
    return sharedService;
});