/**
 * Created by epridonoff on 11/17/14.
 */


$.get('/api/auth/isauth').success(function(res) {

    if (res.isAuthenticated) {

        console.log('authenticated');
    } else {

        //window.location.replace('/sso/sso.py');
        window.location.replace('/app/main/'); // used when sso is disabled, when enabling sso comment out this line
    }

});