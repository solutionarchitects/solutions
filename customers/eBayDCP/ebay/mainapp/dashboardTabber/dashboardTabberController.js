/**
 * Created by Admin on 22-Oct-14.
 */
/**
 * Created by Admin on 14-Oct-14.
 */

var dashboardsTabber = angular.module('dashboardTabber', []);

dashboardsTabber.controller('currentDashboardCtrl',['$scope','sharedApp', function($scope, sharedApp){
    $scope.$on('dashboardUpdated', function(event, dashboard) {
        $scope.currentDashboard = dashboard;

        //loadFilterList(sharedApp.currentDashboard.filtersFile);
    });

    $scope.catchClick = function(){
            debugger

    }

}]);

dashboardsTabber.controller('dashboardTabberController', ['$scope', 'sharedApp', 'angularLoad','dashboardsModel', function ($scope, sharedApp, angularLoad, dashboardsModel) {

    $scope.isBusy = false;
    function getPrism() {
        return $$get(window, 'frames.dashboard.prism');
    }
    $scope.changeDashboard = function(){

        var currentDash  = getCurrentDashboardInstance();
        if((currentDash && (currentDash.refreshing || widgetsRefreshing(currentDash)) ) || $scope.isBusy){
            console.log('Busy');
            return;
        }
        $scope.isBusy = true;
        //window.isBusy = true;
        window.dashboardFinisedRefreshing = false;
        $scope.selectDashboard(this)
    };

    function widgetsRefreshing(dashboard){
        var widgets = dashboard.widgets.$$widgets
        for(var i=0;i<widgets.length;i++){
            if(widgets[i].refreshing){
                return true;
            }
        }
        return false;
    }
    $scope.$on('dashboardsModel::dashboardList', function(event, dashboards) {
        if(DEBUG_MODE){
            console.log('dashboardlist boardcast captured');
        }
        $scope.dashboards = dashboards
        registerDashboards();
    });
    $scope.$on('stateUpdated', function(event, state) {

        if(DEBUG_MODE){
            console.log('stateUpdated');
        }
        $scope.isBusy = state
    });


    $scope.embedOptions = {
        embed: true,
        header: false,
        rightPane: false,
        leftPane: false,
        toolbar: false,
        volatile: true
    };

    function getCurrentDashboardInstance() {
        return $$get(window, 'frames.dashboard.prism.activeDashboard');
    }
    $scope.selectDashboard = function(dashboard){

        ($scope.currentDashboard) && ($scope.currentDashboard.selected = '');
        dashboard.selected = 'selected';
        sharedApp.updateCurrentDashboard(dashboard);
//        $scope.currentDashboard = dashboard;
    };

    $scope.embedString = function(header, rightpane,leftpane,toolbar,volatile){
        var embedString = '';
        embedString += '?embed='+  $scope.embedOptions.embed;
        embedString += rightpane || $scope.embedOptions.rightPane ? "" : "&r="+  $scope.embedOptions.rightPane;
        embedString += leftpane || $scope.embedOptions.leftPane ? "" : "&l="+  $scope.embedOptions.leftPane;
        embedString += header || $scope.embedOptions.header ? "" : "&h="+  $scope.embedOptions.header;
        embedString += toolbar || $scope.embedOptions.toolbar ? "" : "&t="+  $scope.embedOptions.toolbar;
        embedString += volatile || $scope.embedOptions.volatile ? "" : "&volatile=" +  $scope.embedOptions.volatile;
        //embedString += "&r=" + Math.random().toFixed(2);
        return embedString
    };

    var QueryString = function () {
        // This function is anonymous, is executed immediately and
        // the return value is assigned to QueryString!
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = pair[1];
                // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [ query_string[pair[0]], pair[1] ];
                query_string[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                query_string[pair[0]].push(pair[1]);
            }
        }
        return query_string;
    };

    var urls = {
        "dsi":"digital spend impact",
        "marketing":"marketing spend vs visits uplift",
        "bsa":"brand spend analysis",
        "csa":"Channel Spend Analysis"
    }


    function registerDashboards(){
        if(DEBUG_MODE){
            console.log('registring dashboards');
        }

        var urlParams = QueryString();
        if(urlParams.d && urls[urlParams.d]){
            var selectedDashboard = _.find($scope.dashboards,function(d){
                return d.caption.toLowerCase() == urls[urlParams.d].toLowerCase();
            })
        }

        for(var i = 0; i< $scope.dashboards.length; i++){
            $scope.dashboards[i].changeDashboard = $scope.changeDashboard;
            $scope.dashboards[i].frameUrl = $scope.dashboards[i].url + $scope.embedString();
            if(selectedDashboard && selectedDashboard.caption.toLowerCase() !=  $scope.dashboards[i].caption.toLowerCase()){
                $scope.dashboards[i].selected = ''
            }
            else if($scope.dashboards[i].selected == 'selected'){
                //sharedApp.updateCurrentDashboard($scope.dashboards[i]);
                selectedDashboard = $scope.dashboards[i]
            }
        }

        if(!selectedDashboard){
            sharedApp.updateCurrentDashboard($scope.dashboards[0]);
        }
        else{
            sharedApp.updateCurrentDashboard(selectedDashboard);
        }
    }
    if(DEBUG_MODE){
        console.log('initialized dashboardTabberController');
    }
}]);


dashboardsTabber.service('dashboardsModel', ['$rootScope','$http',function ($rootScope,$http) {


    this.loadDashboardList = function(){

        $http({
            method: 'get',
            url: 'mainapp/dashboardTabber/dashboardList.json',
            headers: {
                'Content-type': 'application/json'
            }
        })
            .success(this.broadcast)
            .error(function (data, status, headers, config) {
                console.log('error loading filters');
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });
    };

    this.broadcast= function(data){

        if(DEBUG_MODE){
            console.log('dashboard list loaded');
            console.log(data.dashboards);
        }
        this.dashboards = data.dashboards;
        $rootScope.$broadcast('dashboardsModel::dashboardList', data.dashboards);
    };

    if(DEBUG_MODE){
        console.log('initialized dashboardsModel');
    }
    this.loadDashboardList();
}]);