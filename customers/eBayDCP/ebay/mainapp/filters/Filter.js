

function Filter(settings){
    if(!settings || typeof settings != 'object'){
        console.log('Invalid settings for Filter');
        return;
    }
    this.caption = settings.caption;
    this.datatype = settings.datatype;
    this.defaultValue = settings.defaultValue;
    this.hasAll = settings.hasAll;
    this.loadDynamic = settings.loadDynamic;
    this.multiSelection = settings.multiSelection;
    this.persistentScope = settings.persistentScope;
    this.loadDynamic = settings.loadDynamic;
    this.scope = settings.scope;
    this.dim = settings.dim;
    this.members = settings.members || [];
    this.groups = settings.groups;
    this.affect = settings.affect;

    if(this.hasAll){
        var newMember = {
            value: 'ALL',
            selected: '',
            isDisabled: '',
            all: true
        };
        this.members.push(newMember);
    }
}
