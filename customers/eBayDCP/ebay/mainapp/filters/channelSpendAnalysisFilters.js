/**
 * Created by Admin on 22-Oct-14.
 */
var datasource = "Global_Brand_Campaign";
var filters = [
    {
        caption: 'period',
        dim: '[v_F_SPENDING_PLANNING.DT_FULL (Calendar)]',
        datatype: 'datetime',
        members: [
            {
                member: 'ytd',
                level: 'years',
                dim: '[v_F_SPENDING_PLANNING.DSC_MONTH]',
                isDisabled: '',
                selected: ''
            },
            {
                member: 'qtd',
                level: 'quarters',
                dim: '[v_F_SPENDING_PLANNING.DSC_RETAIL_WEEK]',
                isDisabled: '',
                selected: ''
            },
            {
                member: 'mtd',
                level: 'months',
                dim: '[v_F_SPENDING_PLANNING.DSC_RETAIL_WEEK]',
                isDisabled: '',
                selected: ''
            },
            {
                member: 'custom',
                level: 'days',
                dim: '[v_F_SPENDING_PLANNING.DSC_RETAIL_WEEK]',
                isDisabled: '',
                selected: '',
                collapsed: ''
            }
        ],
        defaultMember: 'ytd'
    },
    {
        caption: 'Country',
        dim: '[v_F_SPENDING_PLANNING.DSC_COUNTRY_CODE]',
        datatype: 'text',
        groups: [
            {
                groupName: 'North America',
                allowedMembers: ['US', 'CA'],
                collapsed: 'collapsed'
            },
            {
                groupName: 'EU',
                allowedMembers: ['DE', 'UK'],
                collapsed: ''
            },
            {
                groupName: 'APAC',
                allowedMembers: ['AU'],
                collapsed: ''
            }
        ],
        defaultMember: 'US',
        affect: [
            '[v_F_SPENDING_PLANNING.DSC_CHANNEL_L1]'
        ]
    },
    {
        caption: 'type',
        dim: '[v_F_SPENDING_PLANNING.DSC_IND_BRAND_CAMPAIGN]',
        datatype: 'text',
        hasAll: true,
        defaultMember: 'ALL'
    },
    {
        caption: 'channel type',
        dim: '[v_F_SPENDING_PLANNING.DSC_CHANNEL_TYPE]',
        //dim: '[v_F_SPENDING_PLANNING.DSC_DIGITAL_CAMPAIGN]',
        datatype: 'text',
        hasAll: true,
        defaultMember: 'ALL'

    },
    {
        caption: 'channel',
        dim: '[v_F_SPENDING_PLANNING.DSC_CHANNEL_L1]',
        datatype: 'text',
        hasAll: true,
        loadDynamic: true,
        multiSelection: true,
        defaultMember: 'ALL',
        scope: [
            '[v_F_SPENDING_PLANNING.DSC_COUNTRY_CODE]'

        ]
    }

]