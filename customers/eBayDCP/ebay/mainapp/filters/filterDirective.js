/**
 * Created by atavdi on 1/5/2015.
 */

filters.directive('group', function() {
    return {
        restrict: 'AE',
        replace: 'true',
        template: '<h3>{{group.groupName}}</h3>',
        link: function(scope, elem, attrs) {

            elem.bind('click', function() {
                elem.css('background-color', 'white');
                scope.$apply(function() {
                    scope.color = "white";
                });
            });
            elem.bind('mouseover', function() {
                elem.css('cursor', 'pointer');
            });
        }
    };
});