/**
 * Created by Admin on 14-Oct-14.
 */

var filters = angular.module('filters', []);

filters.service('filtersModel', ['$rootScope',  '$http', function ($rootScope, $http) {

    this.loadFilterList = function (url) {

//        this.blockFilterPane();
        $http({
            method: 'get',
            url: url,
            headers: {
                'Content-type': 'application/json'
            }
        })
            .success(this.broadcast)
            .error(function (data, status, headers, config) {
                console.log('error loading filters');
            });
    };

    this.broadcast = function (data) {
        if (DEBUG_MODE) {
            console.log('Filters Loaded');
            console.log(data.filterList);
        }
        if (!data.filterList) {
            console.log('Could not find filter list')
        }
        $rootScope.$broadcast('filtersModel::filtersList', data.filterList);
    };

    this.dashboardChanged = function (dashboard) {
        this.currentDashboard = dashboard
        this.loadFilterList(dashboard.filterList);
    };

    if (DEBUG_MODE) {
        console.log('Initialized Filter service');
    }
}]);


filters.controller('filtersController', ['$scope', 'sharedApp', 'filtersModel', function ($scope, sharedApp, filtersModel) {

    $scope.datasource = "Northwind";
    $scope.YEARS_CAPTION = 'y';
    $scope.QUARTERS_CAPTION = 'q';
    $scope.MONTHS_CAPTION = 'm';
    $scope.FOURW_CAPTION = '4w';
    $scope.FOURM_CAPTION = '4m';
    $scope.CUSTOM_CAPTION = 'custom';

    //listen when dashboard changed
    $scope.$on('dashboardUpdated', function (event, dashboard) {

        $scope.currentDashboard = dashboard;
        filtersModel.dashboardChanged(dashboard);

    });

    //listen to when filter list is loaded
    $scope.$on('filtersModel::filtersList', function (event, filterList) {
        $scope.filters = [];
        //$scope.filters = filters;
        prepareFiltersQuery(filterList);
    });

    $scope.applyDashboardFilter = function (filter, selectedMember, $event, changeDynamicFilters) {

        $event && $event.stopPropagation();

        //"digital spend impact" requires "drill up" when filter is changed
        if ($scope.currentDashboard.caption.toLowerCase() == "digital spend impact") {
            checkIfDrillUpIsNeeded(filter, selectedMember)
        }

        // selectedMember can be name of the member or member object
        if (typeof selectedMember == 'string') {
            selectedMember = $scope.getMember(filter, selectedMember)
        }
        if (!selectedMember) {
            console.log('selected member not found');
            return;
        }
        //check if selected member is disabled - currently not needed since we omitted that option
        if (selectedMember.isDisabled.toLowerCase() == 'disabled') {
            return;
        }

        // check if filter is multiselection
        if (filter.multiSelection) {
            if (selectedMember.all && filter.lastSelected[0].all || (filter.lastSelected.length == 1 && selectedMember.value.toLowerCase() == filter.lastSelected[0].value.toLowerCase())) return;
            $scope.tryUnselectMember(filter, selectedMember);
            var dashboardFilter = createFilter(filter, selectedMember);
            if (dashboardFilter == null)  return;

        }
        else if (existingFilters(filter, selectedMember)) return;
        else {
            selectMember(filter, selectedMember);

            var dashboardFilter = createFilter(filter, selectedMember);
            if (dashboardFilter == null)  return;
        }
        if (changeDynamicFilters == undefined || changeDynamicFilters == true) {
            $scope.affectDynamicFilters(filter);
        }

        $scope.filterDashboard(dashboardFilter);

    };

    $scope.filterDashboard = function (filter, refresh, save) {

        var prism = getPrism();
        if (!prism) {
            console.log('prism not found');
            return;
        }
        var currentFilters = prism.$ngscope.dashboard.filters.$$items;

        filter = checkCurrentFilters(currentFilters, filter);

        if (!filter || filter.length == 0) {
            return;
        }

        refresh = (refresh ==  undefined) ? true : refresh;
        save = (save ==  undefined) ? false : save;


        try {
            prism.$ngscope.dashboard.filters.update(filter, {save: save, refresh: refresh})
        }
        catch (e) {
            console.log('Failed to filter dashboard')
        }
    };

    Filter.prototype.applyFilter = $scope.applyDashboardFilter;

    // used only in digital spedn impact - drill up when 2 channels or above is selected
    function checkIfDrillUpIsNeeded(filter) {
        if (filter.caption.toLowerCase() == 'period') return;

        var widgets = getCurrentDashboardWidgets({type: 'quadrant'});
        if (!widgets) return;
        var channelDimension = "[Dim_COUNTRY_CHANNEL_TYPE.DSC_CHANNEL_L1]";

        for (var i = 0; i < widgets.length; i++) {
            var currentPanel = widgets[i].metadata.panel('Rows');
            if (!currentPanel) continue;
            var currentJaql = currentPanel.items[0].jaql;
            if (currentJaql.dim.toLowerCase() == channelDimension.toLowerCase()) continue;
            currentJaql.dim = channelDimension;
            var column = channelDimension.substring(channelDimension.lastIndexOf('.') + 1, channelDimension.length - 1);
            var title = column;
            currentJaql.column = column;
            currentJaql.title = title;
        }
    }


    function getPrism() {
        return $$get(window, 'frames.dashboard.prism');
    }

    function getCurrentDashboardInstance() {
        return $$get(window, 'frames.dashboard.prism.activeDashboard');
    }


    // retrieve widgets from dashbaord
    // can be used with any attribute for example: getCurrentDashboardWidgets({type:chart/line}) or getCurrentDashboardWidgets({desc: "NON BRAND", type:chart/line})
    // no arguments or invalid arguments will return all widgets in the dashbaord
    function getCurrentDashboardWidgets(obj) {
        try {
            var widgets = $$get(window, 'frames.dashboard.prism.activeDashboard.widgets.$$widgets');
            if (!obj || typeof obj != 'object') {
                return widgets;
            }
            if (!widgets || widgets.length == 0) {
                return [];
            }

            var found = true;
            var widgetsToReturn = [];
            for (var i = 0, size = widgets.length; i < size; i++) {
                found = true
                for (var key in obj) {
                    if (widgets[i].hasOwnProperty(key) && (typeof widgets[i][key] == 'string' && typeof obj[key] == 'string' && obj[key].toLowerCase() !== widgets[i][key].toLowerCase() || obj[key] != widgets[i][key])) {
                        found = false;
                        break;
                    }
                }
                if (found) {
                    widgetsToReturn.push(widgets[i]);
                }
            }
            return widgetsToReturn;
        }
        catch (e) {
            console.log('Error occured in ' + arguments.callee.name + ' ' + e);
            return [];
        }
    }

    function createMultiSelectionFilter(filter) {
        var dashboardFilter = {
            jaql: {
                dim: filter.dim,
                datatype: filter.datatype,
                filter: {}
            }
        };

        dashboardFilter.jaql.filter.members = filter.lastSelected.map(function (member) {
            return member.value;
        });
        return dashboardFilter;
    }

    function existingFilters(filter, selectedMember) {

        return filter.lastSelected == selectedMember;
    }

// applies only for multi selection filters
    $scope.tryUnselectMember = function (filter, selectedMember) {

        for (var i = filter.lastSelected.length - 1; i >= 0; i--) {
            if (filter.lastSelected[i].value.toLowerCase() == selectedMember.value.toLowerCase() && selectedMember.value.toLowerCase() != 'all') {
                filter.lastSelected[i].selected = '';
                filter.lastSelected.splice(i, 1);
                return
            }
        }
        selectMember(filter, selectedMember);
    };

    function createDynamicLoadingFiltersQuery(jaqlQuery, filter) {

        for (var j = 0; j < filter.scope.length; j++) {
            var scopeJaql = getScopeQueryTemplate(filter.scope[j]);

            var scopeFilter = getLastSelectedMembersFromFilter(getFilter({dim: filter.scope[j]}), true);

            if (!scopeFilter || scopeFilter[0].toLowerCase() == 'all') return false;
            scopeJaql.filter = {
                members: scopeFilter
            };
            jaqlQuery.metadata.push(scopeJaql)
        }
        if (filter.persistentScope && filter.persistentScope.length > 0) {
            jaqlQuery.metadata = jaqlQuery.metadata.concat(filter.persistentScope[j])
        }

        var filterDim = {
            "dim": filter.dim
        };
        jaqlQuery.metadata.push(filterDim);

    }

    $scope.affectDynamicFilters = function (filter) {

        var jaql = {};
        jaql.datasource = $scope.datasource;
        jaql.metadata = [];

        if (Array.isArray(filter)) {
            createMetadataForDynamicFilters(jaql, filter)

        }
        else {
            createMetadataForDynamicFilters(jaql, [filter])

        }

        if (jaql.metadata.length < 1) {
            $.unblockUI();
            return;
        }

        var data = JSON.stringify(jaql);


        $.ajax({
            url: '/jaql/query',
            type: 'POST',
            async: false,
            data: data,
            success: $scope.updateFilters
        })

    };

    function createMetadataForDynamicFilters(jaql, filter){
        var i;
        for (i = 0; i < filter.length; i++) {
            if (filter[i].affect &&  filter[i].affect.length > 0) {

                var scopeJaql = {};
                scopeJaql.panel = "scope";
                scopeJaql.dim = filter[i].dim;
                var scopeFilter = $$get(filter[i], 'lastSelected.value');
                var members = scopeFilter ? [scopeFilter] : [];
                scopeJaql.filter = {
                    members: members
                };
                jaql.metadata.push(scopeJaql)
            }

        }
        // no scope
        if(jaql.metadata.length < 1){
            return;
        }
        for (i = 0; i < $scope.filters.length; i++) {
            if ($scope.filters[i].loadDynamic == true) {
                var jaqlDim = {
                    "dim": $scope.filters[i].dim
                };
                jaql.metadata.push(jaqlDim);
            }
            if ($scope.filters[i].persistentScope) {
                for (var j = 0, size = $scope.filters[i].persistentScope.length; j < size; j++) {
                    jaql.metadata.push($scope.filters[i].persistentScope[j])
                }
            }
        }
    }

    //get the last selected members, some filters can have multiple last selected,
    //multiple selection filters will return array anyway, in case we need
    //single last selected as array (for filter members for example) then pass returnArray = true
    function getLastSelectedMembersFromFilter(filter, returnArray) {
        if (!filter) throw "Invalid arguments to " + arguments.callee.name;
        try {
            if (Array.isArray(filter.lastSelected)) {
                return filter.lastSelected.map(function (member) {
                    return member.value;
                })
            }
            else {
                var lastSelected = $$get(filter, 'lastSelected.value');
                if (lastSelected) {
                    if (returnArray) lastSelected = [lastSelected];
                    return lastSelected;
                }
            }
            return null;
        }
        catch (e) {
            console.log('Failed To retrieve last selected members ' + e);
        }
    }

    function getScopeQueryTemplate(dim) {
        if (!dim || dim == '') throw "Invalid arguments to " + arguments.callee.name;
        var scopeMember = {
            panel: "scope",
            dim: dim,
            filter: {}
        };

        return scopeMember;
    }

    function getFilterMember(hasAll) {
        var newMember = {
            selected: '',
            isDisabled: '',
            value: ''

        };
        if (hasAll) {
            newMember.value = 'ALL';
            newMember.all = true;
        }
        return newMember;
    }

    $scope.updateFilters = function (data) {
        var i,j;
        for (i = 0; i < data.metadata.length; i++) {
            var filter = getFilter({dim: data.metadata[i].dim});
            if (!filter) continue;

            var isSelectedRemoved = false;

            //j > 0 because ALL member is always relevant
            for (j = filter.members.length - 1; j > 0; j--) {
                if (!memberRelevant(data.values, filter.members[j].value, i)) {
                    if (filter.members[j].selected.toLowerCase() == 'selected') {
                        removeMemberFromLastSelected(filter, filter.members[j]);
                        isSelectedRemoved = true;
                    }
                    filter.members.splice(j, 1);
                }
            }

            for (j = data.values.length - 1; j >= 0; j--) {
                if (newValueRelevant(data.values[j][i].text, filter.members)) {
                    var newMember = {
                        value: data.values[j][i].text,
                        selected: '',
                        isDisabled: ''
                    };
                    filter.members.push(newMember);
                }
            }

//          selectMemeber(filter,$scope.getMember(filter,filter.defaultMember));
            if (filter.lastSelected && filter.lastSelected.length == 0 || isSelectedRemoved) {
                $scope.applyDashboardFilter(filter, $scope.getMember(filter, filter.defaultValue))
            }
        }

        sortMembers();
        //$scope.$apply();
        $.unblockUI();
    };

    function sortMembers() {

        var filterToSort = _.find($scope.filters, function (f) {
            return (f.dim.indexOf('DSC_CHANNEL_L1') > -1)
        });
        if (!filterToSort) return;

        var prism = getPrism();
        if (!prism) return;
        var firstElement = filterToSort.members.shift();

        filterToSort.members.sort(function (a, b) {
            return prism.ebayGlobals.channels[a.value.toLowerCase()].index - prism.ebayGlobals.channels[b.value.toLowerCase()].index
        });
        filterToSort.members.unshift(firstElement);
    }

    function removeMemberFromLastSelected(filter, member) {
        if (Array.isArray(filter.lastSelected)) {
            for (var i = filter.lastSelected.length - 1; i >= 0; i--) {
                if (filter.lastSelected[i] == member) {
                    filter.lastSelected.splice(i, 1);
                }
            }
        }
        else if (filter.lastSelected == member) {
            filter.lastSelected = '';
        }
    }

    function memberRelevant(values, value, index) {
        for (var j = 0; j < values.length; j++) {
            if (values[j][index].text.toLowerCase() == value.toLowerCase()) {
                return true;
            }
        }
        return false;
    }

    function newValueRelevant(value, members) {
        for (var j = 0; j < members.length; j++) {
            if (members[j].value.toLowerCase() == value.toLowerCase()) {
                return false;
            }
        }
        return true;
    }

    $scope.getMemberByName = function (filter, name) {
        if(filter.groups && filter.groups.length){
            for (var i = 0; i < filter.groups.length; i++) {
                for (var j = 0; j < filter.groups[i].members.length; j++) {
                    if (filter.groups[i].members[j].value.toLowerCase() == name.toLowerCase()) {
                        return filter.groups[i].members[j];
                    }
                }

            }
        }
        else{
            for (var i = 0; i < filter.members.length; i++) {
                if (filter.members[i].value.toLowerCase() == name.toLowerCase()) {
                    return filter.members[i];
                }
            }
        }

    };

    $scope.getFilterByDim = function (dim) {
        for (var i = 0; i < $scope.filters.length; i++) {
            if (dim.toLowerCase() == $scope.filters[i].dim.toLowerCase()/* || $scope.filterMap[dim] == $scope.filters[i].dim*/) {
                return $scope.filters[i];
            }
        }
    };

    function createFilter(filter, selectedMember) {
        var dashboardFilter;
        dashboardFilter = {
            jaql: {
                dim: filter.dim,
                datatype: filter.datatype,
                filter: {}
            }
        };

        if (filter.datatype.toLowerCase() == 'datetime') {
            dashboardFilter.jaql.filter = createDateTimeFilter(selectedMember);
            if (dashboardFilter.jaql.filter == null) {
                return null;
            }
            dashboardFilter.jaql.level = selectedMember.level;

        }
        else if (selectedMember.all) {
            dashboardFilter.jaql.filter.all = true;
        }
        else if (filter.multiSelection) {
            dashboardFilter.jaql.filter.members = filter.lastSelected.map(function (member) {
                return member.value;
            })
        }
        else {
            dashboardFilter.jaql.filter.members = [selectedMember.value];
        }

        return dashboardFilter;
    }

    function getCaseInsensetiveRegex(str) {
        if (typeof str == "string") {
            return new RegExp('^' + str + '$', 'i');
        }
        console.log('Invalid arguments to ' + arguments.callee.name);
        return new RegExp('^$');
    }

    function createDateTimeFilter(selectedMember, range) {
        var currentDate;
        var newFilter;
        var yearOffset = -17;
        if (getCaseInsensetiveRegex($scope.YEARS_CAPTION).test(selectedMember.value)) {

            //currentDate = new Date(new Date().getFullYear(), 0, 1);
            currentDate = new Date(new Date().getFullYear()+yearOffset, 0, 1);

            currentDate = currentDate.ToFilterFormat();
            newFilter = {
                members: [currentDate]
            };
            changeDimension(selectedMember);
        }
        else if (getCaseInsensetiveRegex($scope.QUARTERS_CAPTION).test(selectedMember.value)) {

            var firstMonthOfQuarter = ((Math.ceil(((new Date()).getMonth() + 1) / 3) * 3) - 3);
            var currentYear = (new Date()).getFullYear()+yearOffset;
            // temp fix for 2015 data, we want to show data from 2014 - QTD should refer to current quarter - until we make buttons
            // for previous quarter we will refer to current year as 2014

            /* start of fix */
            var previousQuarterFirstMonth = (firstMonthOfQuarter - 3 < 0) ?  (currentYear-- && 10) : firstMonthOfQuarter - 3;
            firstMonthOfQuarter = previousQuarterFirstMonth;
            /* end of fix - should be removed after adding buttons*/

            currentDate = new Date(currentYear, firstMonthOfQuarter, 1);
            currentDate = currentDate.ToFilterFormat();
            newFilter = {
                members: [currentDate]
            };
            changeDimension(selectedMember);
        }
        else if (getCaseInsensetiveRegex($scope.MONTHS_CAPTION).test(selectedMember.value)) {

            var currentMonth = (new Date()).getMonth();
            var currentYear = (new Date()).getFullYear()+yearOffset;

            /* start of fix */
            var previousMonth = (currentMonth - 1 < 0) ?  (currentYear-- && 11) : currentMonth - 1;
            currentMonth = previousMonth;
            /* end of fix - should be removed after adding new buttons*/

            currentDate = new Date(currentYear, currentMonth, 1);
            currentDate = currentDate.ToFilterFormat();

            newFilter = {
                members: [currentDate]
            };
            changeDimension(selectedMember);
        }
        else if (getCaseInsensetiveRegex($scope.CUSTOM_CAPTION).test(selectedMember.value) && range) {

            newFilter = range;
            changeDimension(selectedMember);
        }
        else if(selectedMember.value.toLowerCase() === $scope.FOURM_CAPTION){
            currentDate = get4MonthsStartDate();
            var endDate = new Date();

            newFilter = {
                from:currentDate.ToFilterFormat(),
                to:endDate.ToFilterFormat()
            }
            changeDimension(selectedMember)
        }
        else if(selectedMember.value.toLowerCase() === $scope.FOURW_CAPTION){
            currentDate = get4WeeksStartDate();
            var endDate = new Date();

            newFilter = {
                from:currentDate.ToFilterFormat(),
                to:endDate.ToFilterFormat()
            }
            changeDimension(selectedMember)

        }
        $scope.updateCustomDatePicker(currentDate, selectedMember.value);
        return newFilter;
    }

    function get4MonthsStartDate(){
        var now = new Date();
        var currentMonth = now.getMonth();
        if(currentMonth > 3){
            return new Date(now.getMonth()-3 +  '/01/' + now.getFullYear()-17);
        }
        else{
            var startMonth;
            switch(currentMonth) {
                case 0:
                    startMonth = 8;
                    break;
                case 1:
                    startMonth = 9;
                    break;
                case 2:
                    startMonth = 10;
                    break;
                case 3:
                    startMonth = 11;
                    break;
            }
            return new Date(startMonth+1 +  '/01/' + (now.getFullYear()-1));
        }

    }
    function getDaysInMiliSeconds(numOfDays){
        return 1000* 60 * 60 * 24 * numOfDays;
    }
    function get4WeeksStartDate(){
        var now = new Date();
        var firstDayOfCurrentWeek  = getSunday(now);
        var fourWeeksInMiliseconds = getDaysInMiliSeconds(28);
        return new Date(firstDayOfCurrentWeek.getTime()-fourWeeksInMiliseconds);
    }

    function getSunday(d) {
        d = new Date(d);
        var day = d.getDay(),
            diff = d.getDate() - day + (day == 0 ? -6:0); // adjust when day is sunday
        return new Date(d.setDate(diff));
    }
    $scope.updateCustomDatePicker = function (date, value) {
        var toDate;
        var fromDate = new Date(date);
        if (value.toLowerCase() == $scope.YEARS_CAPTION) {
            toDate = new Date(fromDate.getFullYear(), 11, 31);
        }
        else if (value.toLowerCase() == $scope.QUARTERS_CAPTION) {
            toDate = new Date(fromDate.getFullYear(), fromDate.getMonth() + 3, 0);
        }
        else if (value.toLowerCase() == $scope.MONTHS_CAPTION) {
            toDate = new Date(fromDate.getFullYear(), fromDate.getMonth() + 1, 0);
        }
        else if(value.toLowerCase() === $scope.FOURM_CAPTION){
            toDate = new Date();
        }
        else if(value.toLowerCase() === $scope.FOURW_CAPTION){
            toDate = new Date();
        }
        if (toDate) {
            //change date since its in json
            $scope.setCustomDate(fromDate, toDate)
        }
    };

    // change the dimensions of all the widgets, if the dimensions already exist do nothing
    function changeDimension(selectedMember) {

        var widgets = getCurrentDashboardWidgets();
        if (widgets) {
            for (var i = 0; i < widgets.length; i++) {
                var currentPanel = (widgets[i].metadata.panel('categories') && widgets[i].type == 'chart/column') ? widgets[i].metadata.panel('categories') : widgets[i].metadata.panel('x-axis');
                /* || widgets[i].metadata.panel('categories')*/
                if (!currentPanel) continue;
//                if(currentPanel.items.length == 0) continue
                var currentJaql = currentPanel.items[0].jaql;
                if (currentJaql.dim == selectedMember.dim && currentJaql.datatype != "datetime") {
                    continue;
                }
                else if(currentJaql.dim.toLowerCase() === selectedMember.dim.toLowerCase() && currentJaql.datatype.toLowerCase()  === selectedMember.datatype.toLowerCase() &&
                    currentJaql.level && selectedMember.dimLevel && currentJaql.level.toLowerCase() == selectedMember.dimLevel.toLowerCase()
                   ){
                    continue;
                }
                else{
                    if(selectedMember.format){
                        if(!currentPanel.items[0].format){
                            currentPanel.items[0].format ={mask:{}}
                        }
                        currentPanel.items[0].format.mask[selectedMember.dimLevel] = selectedMember.format;
                    }
                    if(selectedMember.datatype != "datetime"){
                        currentJaql.level = undefined;
                    }
                    else{
                        currentJaql.level = selectedMember.dimLevel;
                    }
                    currentJaql.datatype = selectedMember.datatype;
                }

                currentJaql.dim = selectedMember.dim;
                var column = selectedMember.dim.substring(selectedMember.dim.lastIndexOf('.') + 1, selectedMember.dim.length - 1);
                var title = column;
                currentJaql.column = column;
                currentJaql.title = title;
                widgets[i].changesMade();
                widgets[i].refresh();
            }
        }
    }

    function updateExternalFilters(dim, member) {

        var filter = getFilter({dim: dim});
        var selectedMember = $scope.getMember(filter, member)
        clearSelections(filter);
        selectMember(filter, selectedMember);
        $scope.$apply();
    }

    function selectMember(filter, selectedMember) {
        selectedMember.selected = 'selected';

        if (filter.lastSelected) {
            if (Array.isArray(filter.lastSelected)) { //multiselection
                if (selectedMember.value.toLowerCase() == 'all') {
                    clearSelections(filter);
                }
                else {
                    removeAllSelection(filter)
                }
                filter.lastSelected.push(selectedMember);
            }
            else {
                filter.lastSelected.selected = '';
                filter.lastSelected = selectedMember;
            }
        }
        else if (filter.multiSelection) {

            filter.lastSelected = [];
            filter.lastSelected.push(selectedMember);
        }
        else {
            filter.lastSelected = selectedMember;
        }

    }

    function clearSelections(filter) {
        var i = filter.lastSelected.length - 1;
        while (filter.lastSelected.length > 0) {
            filter.lastSelected[i].selected = '';
            filter.lastSelected.pop();
            i--
        }
    }

    function removeAllSelection(filter) {
        for (var i = 0; i < filter.lastSelected.length; i++) {
            if (filter.lastSelected[i].value.toLowerCase() == 'all') {
                filter.lastSelected[i].selected = '';
                filter.lastSelected.splice(i, 1);
            }
        }
    }

    $scope.getMember = function (filter, selectedMember) {
        if (filter.groups && filter.groups.length > 0) {
            for (var i = 0; i < filter.groups.length; i++) {
                for (var j = 0; j < filter.groups[i].members.length; j++) {
                    if (filter.groups[i].members[j].value.toLowerCase() == selectedMember.toLowerCase()) {
                        return filter.groups[i].members[j];
                    }
                }
            }
        }
        else {
            for (var i = 0; i < filter.members.length; i++) {
                if (filter.members[i].value.toLowerCase() == selectedMember.toLowerCase()) {
                    return filter.members[i];
                }
            }
        }
        return null;
    };

    function checkCurrentFilters(currentFilters, filters) {

        if (Array.isArray(filters)) {
            var filtersCopy = filters.slice(0);
            for (var i = filters.length - 1; i >= 0; i--) {
                for (var j = currentFilters.length - 1; j >= 0 && i >= 0; j--) {
                    if (currentFilters[j].jaql.dim.toLowerCase() == filters[i].jaql.dim.toLowerCase()) {
                        if (isFilterRelevant(currentFilters[j], filters[i])) {
                            currentFilters.splice(j, 1);
                        }
                        else {
                            filtersCopy.splice(i, 1);
                        }

                    }
                }
            }
            return filtersCopy;
        }
        //filters is not an array - single filter
        else {
            for (var j = currentFilters.length - 1; j >= 0; j--) {
                if (currentFilters[j].jaql.dim.toLowerCase() == filters.jaql.dim.toLowerCase()) {
                    if (isFilterRelevant(currentFilters[j], filters)) {
                        currentFilters.splice(j, 1);
                    }
                    else {
                        return null
                    }

                }
            }
        }
        return filters;
    }

    function isFilterRelevant(currentFilter, filter) {
        if (currentFilter.jaql.filter.all && filter.jaql.filter.all) {
            return false
        }
        else if (currentFilter.jaql.filter.all || filter.jaql.filter.all) {
            return true
        }
        else if (currentFilter.jaql.filter.from && currentFilter.jaql.filter.to && filter.jaql.filter.from && filter.jaql.filter.to) {
            return !(currentFilter.jaql.filter.from == filter.jaql.filter.from && currentFilter.jaql.filter.to == filter.jaql.filter.to &&
            filter.level == currentFilter.level);
        }
        else if ((filter.jaql.filter.from && filter.jaql.filter.to) || (currentFilter.jaql.filter.from && currentFilter.jaql.filter.to)) {
            return true;
        }
        else if (currentFilter.jaql.dim.indexOf('Calendar') > -1 && filter.jaql.dim.indexOf('Calendar') > -1) {
            if (currentFilter.jaql.filter.members[0].getMonth) {
                // im creating new date here so the date object can inherit the custom function i created
                return !(new Date(currentFilter.jaql.filter.members[0]).ToFilterFormat() == filter.jaql.filter.members[0] &&
                currentFilter.jaql.level.toLowerCase() == filter.jaql.level.toLowerCase());
            }
        }
        return !filter.jaql.filter.members.equals(currentFilter.jaql.filter.members);
    };


    $scope.isCustom = function (filter) {

        return filter.value.toLowerCase() != $scope.CUSTOM_CAPTION;
        //return filter.member.toLowerCase() != 'custom';
    };

    $scope.toggleDateRange = function (date, $event, filter) {

        if(typeof date == 'string') {
            date = $scope.getMember(filter, date);
        }
        if (date.isDisabled == 'disabled') return;
        if(date.value.toLowerCase() == 'custom' && date.selected.toLowerCase() == 'selected'){

        }
        var collapsibleElement = $('.customDates');
        if ($($event.currentTarget).hasClass('selected') || (!($(collapsibleElement).css('display') == 'block') && date.collapsed == undefined)) return
        //$($event.currentTarget).addClass('selected');
        $(collapsibleElement).slideToggle('fast', 'linear');
    };

    $scope.toggleCollapsed = function (group, $event) {

        if ($('.selected', $event.currentTarget).length > 0) return;
        group.collapsed = group.collapsed.toLowerCase() == 'collapsed' ? '' : 'collapsed';
        var collapsibleElement;

        collapsibleElement = $($event.currentTarget).children('.collapsible');

        $(collapsibleElement).slideToggle('fast', 'linear');
    };

    function prepareFiltersQuery(filterList) {
//        $scope.setDatePicker();

        for (var i = 0; i < filterList.length; i++) {
            $scope.filters.push(new Filter(filterList[i]));
        }

        var jaql = {};

        jaql.datasource = $scope.datasource;
        jaql.metadata = [];
        for (var i = 0; i < $scope.filters.length; i++) {
            if ($scope.filters[i].datatype == 'datetime') continue;
            var jaqlDim = {
                "dim": $scope.filters[i].dim
            };
            jaql.metadata.push(jaqlDim);
        }

        var data = JSON.stringify(jaql);
        $.ajax({
            url: '/jaql/query',
            type: 'POST',
//            async:false,
            data: data,
            success: populateFilters
        })
    }

    function populateFilters(data) {
        var i, j;
        //$scope.filters = filters;
        for (i = 0; i < $scope.filters.length; i++) {
            //$scope.filters[i].method = $scope.applyDashboardFilter;
            if ($scope.filters[i].datatype == 'datetime') continue; // datetime filter already have pre-defined members

            $scope.filters[i].index = getMetadataIndex($scope.filters[i].dim, data.metadata);
        }

        for (j = 0; j < data.values.length; j++) {
            for (i = 0; i < $scope.filters.length; i++) {
                if ($scope.filters[i].datatype == 'datetime' || $scope.filters[i].loadDynamic) continue;

                if (_.find($scope.filters[i].members, function (member) {
                        return member.value.toLowerCase() == data.values[j][$scope.filters[i].index].text.toLowerCase();
                    }) == undefined) {
                    var newMember = {
                        value: data.values[j][$scope.filters[i].index].data,
                        selected: '',
                        isDisabled: ''
                    };

                    $scope.filters[i].members.push(newMember);
                }
            }
        }

        handleGroupedFilters();
        applyDefaults();
    }

    function applyDefaults() {
        var currentTime = new Date();
        var maxDelayInSeconds = 30;

        var waitForPrism = setInterval(function () {
            if ((((new Date) - currentTime) / 1000) > maxDelayInSeconds) {
                clearInterval(waitForPrism);
                prismTimeout();
            }
            var widgets = $$get(window, 'frames.dashboard.prism.activeDashboard.widgets.$$widgets');
            if (widgets && $$get(window, 'frames.dashboard.prism.activeDashboard.oid') === $scope.currentDashboard.id) {

                var prism = getPrism();
                prism.updateOuterFilters = updateExternalFilters;
                prism.ruvClicked = ruvClicked;
                prism.ruvSelectionRemoved = ruvSelectionRemoved;
                clearInterval(waitForPrism);
                $scope.setDatePicker();

                var defaultFilters = [];
                for (var i = 0; i < $scope.filters.length; i++) {
                    if ($scope.filters[i].defaultValue) {

                        var member = $scope.getMember($scope.filters[i], $scope.filters[i].defaultValue);
                        defaultFilters.push(createFilter($scope.filters[i], member));
                        selectMember($scope.filters[i], member);

                        if ($scope.filters[i].datatype == "datetime") {
                            changeDimension($scope.getMember($scope.filters[i], $scope.filters[i].defaultValue))
                        }
                    }
                }
                $scope.affectDynamicFilters($scope.filters);

                //console.log($scope.currentDashboard.id);
                //console.log(getCurrentDashboardInstance().oid);

                $scope.filterDashboard(defaultFilters,true,true);
                $scope.$apply(testAfterApply);
                setAppState(false);

            }
        }, 10);
    }

    function setDashboardRefreshEndHandler(){
        var dashboard = getCurrentDashboardInstance();
        dashboard.on('refreshend',function(){
            setAppState(false);
        })

    }

    function testAfterApply(){

    }

    function setAppState(state) {

        //console.log($scope.$$phase);
        window.isBusy = false;
        sharedApp.updateState(state);
    }

    //function selectDefaults() {
    //    var defaultFilters = [];
    //    for (var i = 0; i < $scope.filters.length; i++) {
    //        if ($scope.filters[i].defaulValue) {
    //
    //            var member = $scope.getMember($scope.filters[i], $scope.filters[i].defaulValue);
    //            defaultFilters.push(createFilter($scope.filters[i], member));
    //            selectMember($scope.filters[i], member);
    //
    //            if ($scope.filters[i].datatype == "datetime") {
    //                changeDimension($scope.getMember($scope.filters[i], $scope.filters[i].defaulValue))
    //            }
    //        }
    //    }
    //    var prism = getPrism();
    //    if (!prism) {
    //        console.log('prism not found');
    //        return;
    //    }
    //    //var currentFilters = prism.$ngscope.dashboard.filters.$$items;
    //
    //    $scope.filterDashboard(defaultFilters);
    //
    //    $scope.$apply();
    //    window.isBusy = false;
    //}

    function ruvSelectionRemoved() {
        $scope.ruvIsSelected = false;
        var filter = getFilter({caption: 'period'});
        enableAllMembers(filter);
        $scope.$apply();
    }

    function prismTimeout() {
        console.log('Unable to Find prism');
        window.top.location = '/app/main/';
    }

    function enableAllMembers(filter) {

        if (filter == null) return;
        if(filter.groups && filter.groups.length){
            for (var i = 0, numOfGroups = filter.groups.length; i < numOfGroups; i++) {
                for (var j = 0, numOfMembers = filter.groups[i].members.length; j < numOfMembers; j++) {
                    filter.groups[i].members[j].isDisabled = '';
                }
            }
        }
        else{
            for (var i = 0, size = filter.members.length; i < size; i++) {
                filter.members[i].isDisabled = '';
            }
        }

    }

    function disableAllOtherMembers(filter, selectedMember) {
        if(filter.groups && filter.groups.length){
            for (var i = 0, numOfGroups = filter.groups.length; i < numOfGroups; i++) {
                for (var j = 0, numOfMembers = filter.groups[i].members.length; j < numOfMembers; j++) {
                    if (selectedMember.value.toLowerCase() !== filter.groups[i].members[j].value.toLowerCase()) {
                       filter.groups[i].members[j].isDisabled = 'disabled';
                    }
                }
            }
        }
        else{
            for (var i = 0, size = filter.members.length; i < size; i++) {
                if (selectedMember.value.toLowerCase() !== filter.members[i].value.toLowerCase()) {
                    filter.members[i].isDisabled = 'disabled';
                }
            }
        }

    }

    function ruvClicked() {
        setTimeout(function () {

            var dates = getWeekStartAndEnd(new Date());
            var filter = getFilter({caption: 'period'});
            var selectedMember = $scope.getMemberByName(filter, $scope.CUSTOM_CAPTION);
            //if(selectedMember !== filter.lastSelected){
            disableAllOtherMembers(filter, selectedMember);
            if (!getCaseInsensetiveRegex('selected').test(selectedMember.selected)) {
                //$('.timeFilter').last().addClass('selected')
                $scope.toggleDateRange(selectedMember, {currentTarget: $('.timeFilter').last()[0]})
            }
            $scope.applyDashboardFilter(filter, selectedMember, null, false);
            $scope.ruvIsSelected = true;
            //}

            var $fromDatePicker = $('.fromDatePicker');
            var $toDatePicker = $('.toDatePicker');

            var from = $fromDatePicker.datepick('getDate');
            var to = $toDatePicker.datepick('getDate');

            var inputFromValue = new Date($fromDatePicker.val());
            var inputToValue = new Date($toDatePicker.val());


            if (from.length > 0 && to.length > 0) {
                from[0].setHours(0, 0, 0, 0);
                to[0].setHours(0, 0, 0, 0);
                if (from[0].getTime() == inputFromValue.getTime() && to[0].getTime() == inputToValue.getTime()) {

                    var timeDiff = Math.abs(from[0].getTime() - to[0].getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    if (diffDays == 6) return;
                }
            }

            $fromDatePicker.datepick('setDate', dates[0]);
            //$('.toDatePicker').datepick('setDate', dates[1]);
            $.datepick.hide($fromDatePicker);

            $scope.$apply();
        }, 1000)
    }

    function getWeekStartAndEnd(start) {
        var dayInMiliSec = 86400000;
        var startDay = 0; //0=sunday, 1=monday etc.
        var d = start.getDay(); //get the current day
        var weekStart = new Date(start.valueOf() - (d <= 0 ? 7 - startDay : d - startDay) * dayInMiliSec); //rewind to start day
        var weekEnd = new Date(weekStart.valueOf() + 6 * dayInMiliSec); //add 6 days to get last day
        return [weekStart, weekEnd];
    }

    $scope.getSelectedMember = function (members) {
        for (var i = 0; i < members.length; i++) {
            if (members[i].selected.toLowerCase() == 'selected') return members[i];
        }
    };

    $scope.setCustomDate = function (fromDate, toDate) {

        $('.fromDatePicker').val(fromDate.ToShowFormat());

        $('.toDatePicker').val(toDate.ToShowFormat());
    };

    $scope.setDatePicker = function (date) {
        var yearsOffset = 2;
        var minDate = new Date(new Date().getFullYear() - yearsOffset, 0, 1);
        var maxDate = new Date(new Date().getFullYear() + yearsOffset, 11, 31);
        var $fromDatePicker = $('.fromDatePicker');
        var $toDatePicker = $('.toDatePicker');
//        $('.fromDatePicker').datepicker();
        $fromDatePicker.datepick({
            onSelect: $scope.onSelectFromDate,
            minDate: minDate,
            maxDate: maxDate,
            onClose: onClose
            //firstDay: 1
        });

        $toDatePicker.datepick({
            onSelect: $scope.onSelectToDate,
            minDate: minDate,
            maxDate: maxDate,
            onClose: onClose
            //firstDay: 1
        });

        var enterCustomDateHandler = function () {

            $('.rangeText', $(this).parent()).css({
                'background-color': '#86b817',
                'color': 'white'
            })
        };

        var leaveCustomDateHandler = function () {

            $('.rangeText', $(this).parent()).css({
                'background-color': 'white',
                'color': '#86b817'
            })
        };

        $fromDatePicker.hover(enterCustomDateHandler, leaveCustomDateHandler)
        $toDatePicker.hover(enterCustomDateHandler, leaveCustomDateHandler)
    };

    function onClose(date) {

    }

    $scope.onSelectFromDate = function (dates) {

        var toDate = checkIfDatePicked('.toDatePicker');
        if ($scope.ruvIsSelected) {
            var weekStartAndEnd = getWeekStartAndEnd(dates[0]);
            var newFromDate = weekStartAndEnd[0];
            if (newFromDate.getTime() != dates[0].getTime()) {
                $.datepick.setDate(this, newFromDate);
//                $('.fromDatePicker').datepick('setDate', newFromDate);
                dates[0] = newFromDate;
            }

            var newToDate = weekStartAndEnd[1];
            if (newToDate.getTime() != toDate.getTime()) {
                $.datepick.setDate($('.toDatePicker'), newToDate);
                toDate = newToDate;
            }
        }

        if (validateDates(dates[0], toDate)) {
            applyDateTimeFilter(dates[0], toDate);
            if ($scope.ruvIsSelected) {
                $.datepick.hide(this);
                $(this).one('click', function (ev) {
                    $.datepick.show(this);
                })
            }
        }
        else {
            indicateError(this);
        }
    };

    function applyDateTimeFilter(fromDate, toDate) {
        var datetimeFilter = getDatetimeFilterTemplate("days");
        var filter = getFilter({caption: 'period'});
        var range = {
            from: fromDate.ToFilterFormat(),
            to: toDate.ToFilterFormat()
        };
        $('.fromDatePicker').removeClass('invalid');
        $('.toDatePicker').removeClass('invalid');
        datetimeFilter.jaql.filter = createDateTimeFilter($scope.getMember(filter, 'custom'), range)
        $scope.filterDashboard(datetimeFilter, true);
    }

    $scope.onSelectToDate = function (dates) {

        var fromDate = checkIfDatePicked('.fromDatePicker');
        var toDate = dates[0];
        if ($scope.ruvIsSelected) {
            var weekStartAndEnd = getWeekStartAndEnd(dates[0]);
            var newToDate = weekStartAndEnd[1]
            if (newToDate.getTime() != dates[0].getTime()) {
                $.datepick.setDate(this, newToDate)
                dates[0] = newToDate;
            }

            var newFromDate = weekStartAndEnd[0];
            if (newFromDate.getTime() != fromDate.getTime()) {
                $.datepick.setDate($('.fromDatePicker'), newFromDate)
//
                fromDate = newFromDate;
                $.datepick.hide(this);
                $(this).one('click', function (ev) {
                    $.datepick.show(this);
                })
            }
            else {
                if (validateDates(fromDate, toDate)) {
                    applyDateTimeFilter(fromDate, toDate);
                    $.datepick.hide(this);
                    $(this).one('click', function (ev) {
                        $.datepick.show(this);
                    })
                }
                else {
                    indicateError(this);
                }
            }
        }
        else {
            if (validateDates(fromDate, toDate)) {
                applyDateTimeFilter(fromDate, toDate);
            }
            else {
                indicateError(this);
            }
        }
    };

    function getDatetimeFilterTemplate(level) {

        try {
            var dateDim = getFilter({datatype: 'datetime'}).dim;
        }
        catch (e) {
            console.log('Failed to retrieve datetime filter dimension');
            dateDim = '[Dim_Date.DT_FULL (Calendar)]'
        }

        return {
            jaql: {
                dim: dateDim,
                datatype: "datetime",
                filter: {},
                level: level || "days"
            }
        }
    }

    function getFilter(obj) {
        if (typeof obj != "object") throw "invalid argument to getFilter";
        var found;
        for (var i = 0, size = $scope.filters.length; i < size; i++) {
            for (var key in obj) {
                if (obj.hasOwnProperty(key) && (obj[key].toLowerCase() == $scope.filters[i][key].toLowerCase())) {
                    found = true;
                }
                else if ($scope.filters[i][key] == undefined) {
                    found = false;
                    break;
                }
            }
            if (found) return $scope.filters[i];
        }
    }

    function indicateError(selector) {
        if ($(selector).hasClass('invalid')) return;
        $(selector).addClass('invalid');
        $scope.$apply();
    }

    function validateDates(fromDate, toDate) {
        return fromDate <= toDate;
    }

    function checkIfDatePicked(selector) {

        var dates = $(selector).datepick('getDate');
        if (dates.length > 0) {
            return dates[0];
        }
        else if ($(selector).val() != "") {
            return new Date($(selector).val());
        }
        return false;
    }

    function handleGroupedFilters() {

        var groupedFilters = _.filter($scope.filters, function (filter) {
            if (filter.groups) {
                return filter.groups.length > 0
            }
            return false
        });

        for (var i = groupedFilters.length - 1; i >= 0; i--) {
            for (var k = groupedFilters[i].groups.length - 1; k >= 0; k--) {

                for (var j = groupedFilters[i].members.length - 1; j >= 0; j--) {
                    if (groupedFilters[i].groups[k].allowedValues.indexOf(groupedFilters[i].members[j].value) > -1) {
                        insertMemberToGroup(groupedFilters[i].groups[k], groupedFilters[i].members.splice(j, 1)[0]);
                    }
                }
            }

        }
    }

    function insertMemberToGroup(group, member) {
        if (group.members == undefined) {
            group.members = [];
        }
        group.members.push(member);
    }

    function getMetadataIndex(dim, metadata) {
        for (var i = 0; i < metadata.length; i++) {
            if (metadata[i].dim.toLowerCase() == dim.toLowerCase()) {
                return i;
            }
        }
        return -1;
    }


    $scope.getInclude = function (filter) {
        if (filter.groups && filter.groups.length > 0 && filter.datatype != "datetime") {
            return '/ebay/mainapp/filters/groupedList.html'
        }

        else if (getCaseInsensetiveRegex('datetime').test(filter.datatype)) {

            //return '/ebay/mainapp/filters/timeFilterTemplate.html'
            return '/ebay/mainapp/filters/groupedTimeFilterTemplate.html'
        }
        return '/ebay/mainapp/filters/memberList.html'
    };
    if (DEBUG_MODE) {
        console.log('Initialized Filter Controller');
    }

}]);