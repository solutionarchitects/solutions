/**
 * Created by Admin on 22-Oct-14.
 */
var datasource = "Global_Brand_Campaign";
var filters = [

    {
        caption: 'Country',
        dim: '[Dim_COUNTRY_CHANNEL_TYPE.DSC_COUNTRY_CODE]',
        datatype: 'text',
        groups: [
            {
                groupName: 'North America',
                allowedMembers: ['US'],
                collapsed: 'collapsed'
            },
            {
                groupName: 'EU',
                allowedMembers: ['DE', 'UK'],
                collapsed: ''
            },
            {
                groupName: 'APAC',
                allowedMembers: ['AU'],
                collapsed: ''
            }

        ],
        defaultMember: 'US',
        affect: [

        ]
    }

]