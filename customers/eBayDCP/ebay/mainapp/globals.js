window.DEBUG_MODE = false;

var $$debug = {};
$$debug.log = function(str, method ,ignoreDebugMode){
    var printingMethod = method || 'dir';
    if(window.DEBUG_MODE || ignoreDebugMode){

        console[printingMethod](str)
    }
};

function defined(root, path) {
    return"undefined" == typeof path || "null" === path || "" === path ? null !== root && "undefined" != typeof root : "undefined" != typeof $$get(root, path);
}


function $$get(root, path, defaultValue) {
    if (_.isString(root) && !defined(path) && (path = root, root = window), !defined(root))
        return defaultValue;
    if (!defined(path) || "" === path)
        return root;
    "string" != typeof path && (path = path.toString());
    var i = 0,
        c = root;
    for (path = path.split("."); i < path.length; i++)
        if (!defined(c = c[path[i]]))
            return defaultValue;
    return c
}

Date.prototype.ToShowFormat = function(){
        var year = this.getFullYear();
        var month = (1 + this.getMonth()).toString();
        month = month.length > 1 ? month : '0' + month;
        var day = this.getDate().toString();
        day = day.length > 1 ? day : '0' + day;
        return month + '/' + day + '/' + year;
};

// format to "2014-01-01T00:00:00";
Date.prototype.ToFilterFormat = function () {

    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
    var dd = this.getDate().toString();

    return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]) + "T00:00:00";
};

// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;
        }
        else if (this[i] != array[i]) {
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;
        }
    }
    return true;
};