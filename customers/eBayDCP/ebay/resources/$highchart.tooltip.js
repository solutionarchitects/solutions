﻿//
// common highcharts wrapper service - tooltip

mod.service('$highchart.tooltip', function ($dom, $filter) {
    
    // number masking
    var numberMask = $filter('number');

    // creates tooltip
            

    // helper functions for generate  the tooltips according the type and series

    var generateTooltipWithTemplate = function (familyType, template, settings) {
        
        var tipper,
            tipscope = {};

        return function (ev) {
            
            if (!tipper) {
                tipper = $dom.tip({
                    scope: tipscope,
                    template: template
                }, {
                    css: 'charts',
                    placement: {
                        place: 'b',
                        anchor: 'l'
                    },
                    initialShowDelay: 0,
                    betweenShowDelay: 0
                });
            }
	        
            switch (familyType) {
                case 'pie':
                    
                    var maxOthersContent = 5,
                        contentLength = $$get(this, 'content.length'),
                        val = (this.y * (this.isNegative === true ? -1 : 1));

                    // content
                    tipscope.seriesName = this.series.options.name;
                    tipscope.noSeriesName = (tipscope.seriesName === '') ? true : false;
                    tipscope.seriesColor = this.color;
                    tipscope.category = this.name;
                    tipscope.isOthersSlice = (this.name === 'Others') ? true : false;
                    tipscope.spaceUnderValues = tipscope.isOthersSlice ? 6 : 0;
                    tipscope.moreInOthers = defined(contentLength) ? contentLength - maxOthersContent : 0;
                    tipscope.hasMoreInOthers = tipscope.moreInOthers > 0;
                    tipscope.othersDetails = contentLength ? this.content.slice(0, maxOthersContent) : null;
                    tipscope.value = (this.mask ? this.mask(val) : numberMask(val));
                    tipscope.percent = Math.round(this.percentage);

                    if ($$get(tipscope, 'othersDetails.length') > 0) {
                        var mask = this.mask || $filter('number');
                        _.each(tipscope.othersDetails, function (item) { if (angular.isNumber(item.y)) { item.y = mask(item.y); } });
                    }
                    
                    // position
                    tipscope.x = $$get(ev, 'currentTarget.tooltipPos.0') || $$get(document, 'mousePosition.x') || $$get(event, 'clientX');
                    tipscope.y = $$get(ev, 'currentTarget.tooltipPos.1') || $$get(document, 'mousePosition.y') || $$get(event, 'clientY');
                    break;

                case 'cartesian':
                case 'polar':
                    // content
                    tipscope.category = this.category;
                    
                    var plotBands = $$get(this, 'series.xAxis.options.plotBands'), numOfPlotBands = $$get(plotBands, 'length');
                    
                    if (numOfPlotBands > 0) {

                        tipscope.hasTwoXAxis = true;

                        for (var i = 0; i < numOfPlotBands; i++) {

                            if (this.x > plotBands[i].from && this.x < plotBands[i].to) {

                                tipscope.plotBand = $$get(plotBands[i], 'label.text');
                                break;
                            }
                        }
                    } else {
                        tipscope.hasTwoXAxis = false;
                    }

                    tipscope.points = [];

                    var series = $$get(this, 'series.chart.series'),
                        stackTotal = this.stackTotal;

                    // multi series
                    if (settings.cartesianShowMultiSeries <= series.length && false) { // false condition to disable shared tooltip
                        
                        for (var i = 0, l = series.length; i < l; i++) {

                            if (series[i].name === 'Navigator') {
                                continue;
                            }

                            var point = {};

                            point.seriesName = series[i].name;

                            var mask = $$get(series[i], 'options.mask');

                            var valueNoMask = $$get(series[i], 'data.' + this.x + '.y');
                            point.value = defined(valueNoMask) ? (defined(mask) ? series[i].options.mask(valueNoMask) : valueNoMask) : null;
                            point.valueColor = defined(this.color) ? this.color : series[i].color;
                            point.showPercentage = defined(stackTotal) && stackTotal !== 0;

                            if (point.showPercentage === true) {
                                point.percentage = Math.round(valueNoMask / stackTotal * 100).toFixed(0) + '%';
                            }

                            if (!isNaN(valueNoMask)) {
                                tipscope.points.push(point);
                            }
                        }

                    }
                    // single series
                    else {
                        var point = {},
                            stackTotal = this.stackTotal;

                        var mask = $$get(this, 'series.options.mask');
                        
                        point.seriesName = $$get(this, 'series.options.onlyMeasuresInSeries') === true ? series[this.x].name : this.series.name;
                        point.value = defined(mask) ? mask(this.y) : this.y;
                        point.valueColor = defined(this.color) ? this.color : this.series.color;
                        point.showPercentage = defined(stackTotal) && stackTotal !== 0;
                        
                        if (point.showPercentage === true) {
                            point.percentage = Math.round(this.y / stackTotal * 100).toFixed(0) + '%';
                        }

                        tipscope.points.push(point);
                    }

                    // position
                    var chartOffset = $(this.series.chart.renderTo).offset();

                    if (this.series.type === 'column' || this.series.type === 'bar') {
                        tipscope.x = $$get(ev, 'currentTarget.tooltipPos.0') || $$get(document, 'mousePosition.x') || $$get(event, 'clientX');
                        tipscope.y = $$get(ev, 'currentTarget.tooltipPos.1') || $$get(document, 'mousePosition.y') || $$get(event, 'clientY');
                    }
                    else {
                        tipscope.x = chartOffset.left + this.series.chart.plotBox.x + this.plotX;
                        tipscope.y = chartOffset.top + this.series.chart.plotBox.y + this.plotY;
                    }

                    break;
                    
                case 'scatter':
                    // content
                    
                    tipscope.memberColor = this.series.color;

                    tipscope.metric1Header = this.series.xAxis.options.dimTitle.text;
                    tipscope.metric1Value = ($$get(this, 'series.xAxis.categories.length') > 0) ? this.series.xAxis.categories[this.x] : (this.xMask ? this.xMask(this.x) : numberMask(this.x));

                    tipscope.metric2Header = this.series.yAxis.options.dimTitle.text;
                    tipscope.metric2Value = ($$get(this, 'series.yAxis.categories.length') > 0) ? this.series.yAxis.categories[this.y] : (this.yMask ? this.yMask(this.y) : numberMask(this.y));

                    tipscope.labelHeader = this.series.chart.series[0].options.labelTitle.text;
                    tipscope.memberPoint = this.pointText;
                    tipscope.hasPoint = defined(tipscope.labelHeader) && tipscope.labelHeader !== ' ';
                    
                    tipscope.colorHeader = this.series.chart.series[0].options.colorTitle.text;
                    tipscope.memberBreakBy = !$$get(this, 'series.chart.series.0.options.isPointName') ? this.series.name : null;
                    tipscope.hasColor = defined(tipscope.colorHeader) && this.series.chart.series[0].options.isDummyColor !== true && tipscope.colorHeader !== ' ';

                    tipscope.sizeHeader = this.series.chart.series[0].options.sizeTitle.text;
                    tipscope.sizeValue = (this.zMask ? this.zMask(this.z) : numberMask(this.z));
                    tipscope.hasSize = defined(tipscope.sizeHeader) && tipscope.sizeHeader !== ' ';


                    tipscope.isColorLastField = tipscope.hasSize === false;
                    tipscope.isPointLastField = tipscope.isColorLastField && tipscope.hasColor === false;
                    tipscope.isYLastField = tipscope.isPointLastField && tipscope.hasPoint === false;

                    tipscope.xPaddingBottom = '8px';
                    tipscope.yPaddingBottom = (tipscope.isYLastField ? '0px' : '8px');
                    tipscope.pointPaddingBottom = (tipscope.isPointLastField ? '0px' : '8px');
                    tipscope.colorPaddingBottom = (tipscope.isColorLastField ? '0px' : '8px');
                    tipscope.sizePaddingBottom = '0px';


                    // position
                    tipscope.x = $$get(ev, 'currentTarget.tooltipPos.0') || $$get(document, 'mousePosition.x') || $$get(event, 'clientX');
                    tipscope.y = $$get(ev, 'currentTarget.tooltipPos.1') || $$get(document, 'mousePosition.y') || $$get(event, 'clientY');

                    break;
            }

            tipper.activate({ x: tipscope.x, y: tipscope.y, space: 0 });

	        this.series.chart.sisenseTipper = { familyType: familyType, type: this.series.type, tipper: tipper, status: 'on' };
        };
    };

    var generateCartesianTooltip = function () {
        return '<div class="cartesian-tooltip">' +
            '<div class="cartesian-tooltip-values" ng-repeat="item in model.points">' + 
                '<div class="cartesian-tooltip-seriesName"> {{item.seriesName}} </div> <div class="cartesian-tooltip-singleLineValue" style="color:{{item.valueColor}}"> <div class="cartesian-tooltip-value"> {{item.value}} </div> <div class="cartesian-tooltip-percentage" data-ng-show="item.showPercentage"> / {{item.percentage}} </div> </div>' +
            '</div>' + // values
            '<div class="highcharts-tooltip-line-separator"></div>' + // line separator
            '<div class="cartesian-tooltip-xAxis"> <div class="cartesian-tooltip-plotBand" data-ng-show="model.hasTwoXAxis"> {{model.plotBand}},&nbsp </div> <div class="cartesian-tooltip-category">{{model.category}} </div> </div>' + // first line - xAxis details
            '</div>';

    };

    var generatePieTooltipTemplate = function () {
        
        return '<div class="pie-tooltip"> <div class="pie-tooltip-headers"> <div class="pie-tooltip-seriesName" data-ng-hide="model.noSeriesName">{{model.seriesName}} &nbsp-&nbsp </div> <div class="pie-tooltip-category"> {{model.category}} </div> </div>' + //first line
            '<div class="pie-tooltip-values" style="color:{{model.seriesColor}}; padding-bottom: {{model.spaceUnderValues}}px;"> <div class="pie-tooltip-value">{{model.value}}</div><div class="pie-tooltip-percentage"> &nbsp/ {{model.percent}}%</div></div>' + // second line
            '<div class="highcharts-tooltip-line-separator" data-ng-show="model.isOthersSlice"></div>' + // line separator
            '<div class="pie-tooltip-others" data-ng-show="model.isOthersSlice"> <div class="pie-tooltip-others-details" ng-repeat="item in model.othersDetails"> {{item.name}}&nbsp:&nbsp{{item.y}} </div> <div class="pie-tooltip-moreInOthers" data-ng-show="model.hasMoreInOthers"> {{model.moreInOthers}} more... </div> </div> </div>'; // third line (others details)
    };

    var generateScatterTooltip = function () {

        return '<div class="scatter-tooltip">' +

            '<div class="scatter-tooltip-xAxis">' +
                '<div class="scatter-tooltip-xValue"> {{model.metric1Value}} </div>' +
                '<div class="scatter-tooltip-xHeader" style="color:{{model.memberColor}}; padding-bottom:{{model.xPaddingBottom}};"> {{model.metric1Header}} </div>' +
                '<div class="highcharts-tooltip-line-separator"></div>' + // line separator
            '</div>' +

            '<div class="scatter-tooltip-yAxis">' +
                '<div class="scatter-tooltip-yValue"> {{model.metric2Value}} </div>' +
                '<div class="scatter-tooltip-yHeader" style="color:{{model.memberColor}}; padding-bottom:{{model.yPaddingBottom}};"> {{model.metric2Header}} </div>' +
                '<div class="highcharts-tooltip-line-separator" data-ng-hide="model.isYLastField"></div>' + // line separator
            '</div>' +

            '<div class="scatter-tooltip-point" data-ng-show="model.hasPoint">' +
                '<div class="scatter-tooltip-pointValue"> {{model.memberPoint}} </div>' +
                '<div class="scatter-tooltip-pointHeader" style="color:{{model.memberColor}}; padding-bottom:{{model.pointPaddingBottom}};"> {{model.labelHeader}} </div>' +
                '<div class="highcharts-tooltip-line-separator" data-ng-hide="model.isPointLastField"></div>' + // line separator
            '</div>' +

            '<div class="scatter-tooltip-color" data-ng-show="model.hasColor">' +
                '<div class="scatter-tooltip-colorValue"> {{model.memberBreakBy}} </div>' +
                '<div class="scatter-tooltip-colorHeader" style="color:{{model.memberColor}}; padding-bottom:{{model.colorPaddingBottom}};"> {{model.colorHeader}} </div>' +
                '<div class="highcharts-tooltip-line-separator" data-ng-hide="model.isColorLastField"></div>' + // line separator
            '</div>' +

            '<div class="scatter-tooltip-size" data-ng-show="model.hasSize">' +
                '<div class="scatter-tooltip-sizeValue"> {{model.sizeValue}} </div>' +
                '<div class="scatter-tooltip-sizeHeader" style="color:{{model.memberColor}}; padding-bottom:{{model.sizePaddingBottom}};"> {{model.sizeHeader}} </div>' +
            '</div>' +

            '</div>';

    };


    // sets the proper tooltip due to its family type
    this.generateTooltip = function (familyType, settings,s,event) {
        
        var template;

        switch (familyType) {

            case 'cartesian':
            case 'polar':
                template = generateCartesianTooltip();
                break;

            case 'pie':
                
                template = generatePieTooltipTemplate();
                break;

            case 'scatter':
                template = generateScatterTooltip();
                break;

            case 'table':
                
                break;

            default:
                break;
        }

        return generateTooltipWithTemplate(familyType, template, settings, event);

    };

})

// settingup DI
.$inject([
    'ux-controls.services.$dom',
    '$filter'
]);
